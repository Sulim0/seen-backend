const { Bachelor } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const bachelors = await Bachelor.findAll();
        res.status(200).json(bachelors);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { bachelor_name } = req.body;
    try {
        const bachelor = await Bachelor.create({
            bachelor_name: bachelor_name
        });
        res.status(200).json(bachelor);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const bachelor = await Bachelor.findByPk(id);
        bachelor.bachelor_name = req.body.bachelor_name || bachelor.bachelor_name;
        await bachelor.save();
        res.status(200).json({ message: 'bachelor updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const bachelor = await Bachelor.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'bachelor deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}