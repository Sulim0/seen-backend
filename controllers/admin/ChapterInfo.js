const fs = require('fs');

module.exports = {
    getChapter: (req, res) => {
        try {
            fs.readFile('chapterInfo.json', 'utf8', function (err, data) {
                if (err) throw err;
                const info = JSON.parse(data);
                return res.status(200).json({
                    chapter: info.chapter
                });
            });
        }
        catch (error) {
            return res.status(500).json({ error });
        }
    },
    setChapter: (req, res) => {
        try {

            const { chapter } = req.body;

            fs.writeFile('chapterInfo.json', JSON.stringify({ chapter }), { flag: 'w' }, function (err) {
                if (err) throw err;
                return res.status(200).json({ message: "Success" });
            });
        }
        catch (error) {
            return res.status(500).json({ error });
        }
    }
}