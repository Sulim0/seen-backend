const { Code, Subject, sequelize, User, Year, Collage, University, Bachelor, Subject_code } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const codes = await Code.findAll({
            attributes: ["id", "code_content", "date_of_activation", "code_name", "code_notes", "expiry_time", "is_active", "agent", "type", "price"],
            include: [{
                model: User,
                as: "user",
                attributes: ["user_fullname", "user_email", "user_phonenumber", "user_type"]
            },
            {
                model: Subject,
                as: "subjects",
                attributes: ["subject_name", "id"],
                through: { attributes: [] },
                include: [{
                    model: Year,
                    as: "year",
                    attributes: ["year_name"],
                    include: [{
                        model: Collage,
                        as: "collage",
                        attributes: ["collage_name"],
                        include: [{
                            model: University,
                            as: 'university',
                            attributes: ["university_name"]
                        }]
                    }]
                }, {
                    model: Bachelor,
                    as: 'bachelor',
                    attributes: ["bachelor_name"]
                }]
            }],
            order: [['id', 'DESC']]
        });
        const data = codes.map((code, index) => ({
            id: code.id,
            code_content: code.code_content,
            code_name: code.code_name,
            code_notes: code.code_notes,
            expiry_time: code.expiry_time,
            is_active: code.is_active,
            agent: code.agent,
            type: code.type,
            price: code.price,
            origin_expiry_time: convertDate(code.date_of_activation, code.expiry_time),
            user_name: code.user ? code.user.user_fullname : "",
            user_email: code.user ? code.user.user_email : "",
            user_phone: code.user ? code.user.user_phonenumber : "",
            user_type: code.user ? code.user.user_type : "",
            subjects: code?.subjects.map((subject) => subject?.subject_name).join(', '),
            subjects_id: code?.subjects.map((subject) => subject?.id),
            level: code?.subjects[0]?.year ? `${code?.subjects[0]?.year?.collage?.collage_name}, ${code?.subjects[0]?.year?.collage?.university?.university_name}, السنة ${code?.subjects[0]?.year?.year_name}` : `${code?.subjects[0]?.bachelor?.bachelor_name}`
        }));
        return res.status(200).json(data);
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.get = async (req, res, next) => {
    try {
        const code = await Code.findOne({
            attributes: ["id", "code_content", "code_name", "code_notes", "expiry_time", "is_active", "agent", "type", "price", "m_university"],
            include: [
                {
                    model: Subject,
                    as: "subjects",
                    attributes: ["subject_name"],
                    through: { attributes: [] },
                    include: [{
                        model: Year,
                        as: "year",
                        attributes: ["year_name", "id"],
                        include: [{
                            model: Collage,
                            as: "collage",
                            attributes: ["collage_name", "id"],
                            include: [{
                                model: University,
                                as: 'university',
                                attributes: ["university_name", "id"]
                            }]
                        }]
                    }, {
                        model: Bachelor,
                        as: 'bachelor',
                        attributes: ["bachelor_name"]
                    }]
                }],
            where: {
                id: req.params.id
            },
            raw: true,
        });
        return res.status(200).json(code);
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.store = async (req, res, next) => {
    const { number, code_name, code_notes, subjects_ids, expiry_time, agent, type, price, m_university } = req.body;
    let array = []
    try {
        for (let i = 0; i < number; i++) {
            const code = await Code.create({
                code_name: code_name,
                code_notes: code_notes,
                expiry_time: expiry_time,
                agent: agent,
                type: type,
                price: price,
                m_university: m_university,
            });
            const subjects = await Subject.findAll({
                where: {
                    id: subjects_ids
                }
            });
            const result = await code.addSubjects(subjects);
            array.push(code);
        }
        const code = await Code.findOne({
            attributes: ["id", "code_content", "code_name", "code_notes", "expiry_time", "is_active", "agent", "type", "price"],
            where: {
                id: array[0].id
            },
            include: [{
                model: User,
                as: "user",
                attributes: ["user_fullname", "user_email", "user_phonenumber", "user_type"]
            },
            {
                model: Subject,
                as: "subjects",
                attributes: ["subject_name"],
                through: { attributes: [] },
                include: [{
                    model: Year,
                    as: "year",
                    attributes: ["year_name"],
                    include: [{
                        model: Collage,
                        as: "collage",
                        attributes: ["collage_name"],
                        include: [{
                            model: University,
                            as: 'university',
                            attributes: ["university_name"]
                        }]
                    }]
                }, {
                    model: Bachelor,
                    as: 'bachelor',
                    attributes: ["bachelor_name"]
                }]
            }],
        });
        const obj = {
            subjects: code.subjects.map((subject) => subject.subject_name).join(', '),
            level: code.subjects[0].year ? `${code.subjects[0].year.collage.collage_name} ${code.subjects[0].year.collage.university.university_name} السنة ${code.subjects[0].year.year_name}` : `${code.subjects[0].bachelor.bachelor_name}`
        };
        array.unshift(obj);
        return res.status(200).json({ codes: array });
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.destroy = async (req, res, next) => {
    console.log(req.body);
    const { ids } = req.body;
    try {
        const codes = await Code.destroy({
            where: {
                id: ids
            }
        });
        return res.status(200).json({ message: "codes deleted successfully" });
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.edit = async (req, res, next) => {
    console.log(req.body);
    const { id } = req.params;
    try {
        const code = await Code.findByPk(id);
        if (!code) {
            return res.status(404).json({ message: "no code found" })
        }
        code.code_name = req.body.code.code_name || code.code_name;
        code.code_notes = req.body.code.code_notes || code.code_notes;
        code.expiry_time = req.body.code.expiry_time || code.expiry_time;
        code.price = req.body.code.price || code.price;
        code.is_active = req.body.is_active;
        if (req.body.formData.length > 0) {
            const codeSubject = await Subject_code.destroy({
                where: {
                    code_id: code.id
                }
            });
            const subjects = await Subject.findAll({
                where: {
                    id: req.body.formData
                }
            });
            const result = await code.addSubjects(subjects);
        }
        await code.save();
        res.status(200).json({ message: 'code updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.updateActive = async (req, res, next) => {
    console.log(req.body);
    const { ids } = req.body
    try {
        const codes = await Code.update({
            is_active: sequelize.literal('NOT is_active')
        }, {
            where: {
                id: ids
            }
        })
        return res.status(200).json({ message: "codes updated successfully" });
    } catch (error) {
        return res.status(500).json(error);
    }
};

const convertDate = (dateOfActivate, expiry_time) => {
    let date1 = new Date();
    let date2 = new Date(dateOfActivate);
    if (dateOfActivate === null) {
        return expiry_time;
    }
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays - 1 + expiry_time;
}