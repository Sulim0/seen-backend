const { Collage, University } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const collages = await Collage.findAll({
            include: [{
                model: University,
                as: 'university',
                attributes: ['university_name', 'university_state']
            }],
            raw: true
        });
        res.status(200).json(collages);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { university_id, collage_name } = req.body;
    try {
        const collage = await Collage.create({
            university_id: university_id,
            collage_name: collage_name
        });
        res.status(200).json(collage);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const collage = await Collage.findByPk(id);
        if (!collage) {
            return res.status(404).json({ message: "no collage found"})
        }
        collage.university_id = req.body.university_id || collage.university_id;
        collage.collage_name = req.body.collage_name || collage.collage_name;
        await collage.save();
        res.status(200).json({ message: 'collage updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const collage = await Collage.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'collages deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}