const { Coupon, Subject, sequelize, User_coupon } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const coupons = await Coupon.findAll({
            attributes: ["id", "coupon_content", "coupon_notes", "expiry_time", "is_active", "limit"],
            include: [{
                model: Subject,
                as: "subjects",
                attributes: ["subject_name"],
                through: { attributes: [] },
            }],
            order: [['id', 'DESC']]
        });
        const data = coupons.map(async(coupon) => ({
            ...coupon.toJSON(),
            stay: coupon.limit - await User_coupon.count({
                where: {
                    coupon_id: coupon.id,
                }
            }),
            subjects: coupon.subjects.map((subject) => subject.subject_name).join(" ,"),
        }));
        const result = await Promise.all(data)
        return res.status(200).json(result);
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.store = async (req, res, next) => {
    const { coupon_content, coupon_notes, expiry_time, limit, subjects_ids } = req.body;
    try {
            const coupon = await Coupon.create({
                coupon_content: coupon_content.toLowerCase(),
                coupon_notes: coupon_notes,
                expiry_time: expiry_time,
                limit: limit
            });
            const subjects = await Subject.findAll({
                where: {
                    id: subjects_ids
                }
            });
            const result = await coupon.addSubjects(subjects);
        return res.status(200).json({ message: "process completed successfully" });
    } catch (error) {
        // if (error.name === se) {
            
        // }
        return res.status(500).json(error.errors[0].message);
    }
};

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
            const codes = await Coupon.destroy({
                where: {
                    id: ids
                }
            });
        return res.status(200).json({ message: "coupon deleted successfully" });
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const coupon = await Coupon.findByPk(id);
        if (!coupon) {
            return res.status(404).json({ message: "no coupon found"})
        }
        coupon.expiry_time = req.body.expiry_time || coupon.expiry_time;
        coupon.limit = req.body.limit || coupon.limit;
        coupon.coupon_notes = req.body.coupon_notes || coupon.coupon_notes;
        coupon.coupon_content = req.body.coupon_content || coupon.coupon_content;
        await coupon.save();
        res.status(200).json({ message: 'coupon updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.updateActive = async (req, res, next) => {
    console.log(req.body);
    const { ids } = req.body
    try {
        const coupons = await Coupon.update({
            is_active: sequelize.literal('NOT is_active')
        }, {
            where: {
                id: ids
            }
        })
        return res.status(200).json({ message: "coupons updated successfully" });
    } catch (error) {
        return res.status(500).json(error.message);
    }
};