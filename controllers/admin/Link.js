const { Link } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const links = await Link.findAll();
        res.status(200).json(links);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { description, url } = req.body;
    try {
        const link = await Link.create({
            url: url,
            description: description
        });
        res.status(200).json(link);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const link = await Link.findByPk(id);
        if (!link) {
            return res.status(404).json({ message: "no link found"})
        }
        link.url = req.body.url || link.url;
        link.description = req.body.description || link.description;
        await link.save();
        res.status(200).json({ message: 'link updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const link = await Link.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'link deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}