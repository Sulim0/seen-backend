const { Manager } = require('../../utils/models');
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
require('dotenv').config();
const { createTransport } = require('../../utils/node mailer/send_mail');

exports.register = async (req, res, next) => {
    const { user_name, password, email } = req.body;
    const code = crypto.randomBytes(3).toString('hex');
    try {
        const manager = await Manager.create({
            user_name: user_name,
            email: email,
            password: password,
            verification_code: code
        });
        const info = createTransport(manager.user_name, code);
        return res.status(200).json(manager);
    } catch (error) {
        return res.status(400).json(error.message);
    }
};

exports.verify = async (req, res, next) => {
    const { code } = req.body;
    const { manager_id } = req.params;
    try {
        const manager = await Manager.findOne({
            where: {
                id: manager_id,
                verification_code: code
            }
        });
        if (!manager) {
            return res.status(404).json({
                message: 'verification code not correct'
            });
        }
        manager.active = true;
        await manager.save();
        return res.status(200).json({ message: 'verification code correct' });
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.login = async (req, res, next) => {
    const { password, email } = req.body;
    try {
        const manager = await Manager.findOne({
            where: {
                email: email,
            }
        });
        if (!manager) {
            return res.status(404).json({ message: "email not found" });
        } else if (manager.active === true) {
            if (manager.password !== password) {
                return res.status(404).json({ message: "password is incorrect" });
            }
            const token = jwt.sign({ id: manager.id.toString(), email: manager.email, password: manager.password }, process.env.SECRET_KEY, {
                expiresIn: "24h",
            });
            return res.status(200).json({
                token: token,
                manager: manager
            });
        } else {
            return res.status(403).json({ message: "verify your account" });
        }
    } catch (error) {
        return res.status(500).json(error.message);
    }
};