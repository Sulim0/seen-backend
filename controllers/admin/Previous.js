const { Collage, University, Subject, Year, Bachelor, Previous } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const previous = await Previous.findAll({
            include: [{
                model: Subject,
                as: 'subject',
                attributes: ["subject_name"],
                include: [{
                    model: Year,
                    as: 'year',
                    attributes: ['year_name'],
                    include: [{
                        model: Collage,
                        as: 'collage',
                        attributes: ['collage_name', 'id'],
                        include: [{
                            model: University,
                            as: 'university',
                            attributes: ["university_name", "university_state", "id"]
                        }]
                    }]
                    
                },
                {
                    model: Bachelor,
                    as: 'bachelor',
                    attributes: ['bachelor_name']
                }],
            }],
            raw: true
        });
        res.status(200).json(previous);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { previous_name, subject_id, previous_notes } = req.body;
    try {
        const previous = await Previous.create({
            previous_name: previous_name,
            subject_id: subject_id,
            previous_notes: previous_notes,
        });
        res.status(200).json(previous);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const previous = await Previous.findByPk(id);
        if (!previous) {
            return res.status(404).json({ message: "no previous found"})
        }
        previous.previous_name = req.body.previous_name || previous.previous_name;
        previous.previous_notes = req.body.previous_notes || previous.previous_notes;
        previous.subject_id = req.body.subject_id || previous.subject_id;
        await previous.save();
        res.status(200).json({ message: 'previous updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const previous = await Previous.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'previous deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.getCoursesForSubject = async (req, res, next) => {
    try {
        const courses = await Previous.findAll({
            where: {
                subject_id: req.params.subject_id
            },
            order: [['sort', 'ASC'], ['createdAt', 'ASC']]
        });
        res.status(200).json(courses);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.sort = async (req, res, next) => {
    const { data } = req.body;
    console.log(data);
    try {
        for (let i = 0; i < data.length; i++) {
            const previous = await Previous.update({
                sort: i + 1
            }, {
                where: {
                    id: data[i].id
                }
            });
        }
        return res.status(200).json({ message: "completed successfully" })
    } catch (error) {
        res.status(500).json(error.message);
    }
}