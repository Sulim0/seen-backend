const { Question, Sub_subject, Previous, Answer, Sub_subject_question, sequelize, Previous_question, Sequelize } = require('../../utils/models');
const fs = require('fs');
// const csv = require('csv-parser');
const XLSX = require('xlsx');
const getAllQuestions = require('./helper/getAllQuestions');
const sub_subject_model = require('../../utils/models/sub_subject_model');

const pwd = "root\\seen\\public\\images\\app\\seen-"
async function downloadImage(url, filename) {
    const response = await axios.get(url, { responseType: 'arraybuffer' });
  
    fs.writeFile( pwd + filename , response.data, (err) => {
      if (err) throw err;
      console.log('Image downloaded successfully!');
    });
  }

const update_image = async (obj) => {
    try{
      let input_img = obj.url;
      let n = input_img.split('/')
      let l = n.length
      let new_link =  "https://drive.usercontent.google.com/u/0/uc?id=" + n[l-2] + "&export=download"
      console.log(new_link)
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      const img_name = 'image-' + uniqueSuffix + '.jpg'
      await downloadImage(new_link, img_name);
      return "http://seen-sy.com:8080/public/images/app/seen-" + img_name
    }
    catch(err){
      console.log(err);
      return "";
    }
  }

exports.index = async (req, res, next) => {
    try {
        res.status(200).json(await getAllQuestions());
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.getOne = async (req, res, next) => {
    const { id } = req.params;
    try {
        const question = await Question.findOne({
            include: [
                {
                    model: Sub_subject_question,
                    as: 'sub_subject_questions',
                    attributes: ["question_id"],
                    include: [{
                        model: Sub_subject,
                        as: 'sub_subject',
                        attributes: ["sub_subject_name", "id"]
                    }]
                },
                {
                    model: Previous_question,
                    as: 'previous_questions',
                    attributes: ["question_id"],
                    include: [{
                        model: Previous,
                        as: 'previous',
                        attributes: ["previous_name", "id"]
                    }]
                },
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ["answer_content", "id", "correctness"]
                }
            ],
            where: {
                id: id
            }
        });
        res.status(200).json(question);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

//* from excel file
exports.store = async (req, res, next) => {
    const { path } = req.file;
    let questions = [];
    let answers = [];
    let arrayOfSubSubjects = [];
    let arrayOfPrevious = [];
    let transaction;
    try {
        transaction = await sequelize.transaction();
        const workbook = XLSX.readFile(path);
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];
        await XLSX.utils.sheet_to_json(sheet).forEach((element) => {
            let an = []
            const question = {
                is_rollable: element["قابل للخلط"] === "yes" ? true : false,
                question_content: element["نص السؤال"],
                question_notes: element["التفسير"],
                is_mcq: element["الخيار الأول"] !== "null" ? true : false,
                previous_ids: JSON.parse(element["الدورة"]),
                sub_subject_ids: JSON.parse(element["التصنيف الفرعي"]),
                url: element["صورة السؤال"]
            }
            console.log(JSON.parse(element["التصنيف الفرعي"]));
            for (const [key, value] of Object.entries(element)) {
                let answer = {};
                if (key === "صورة4" || key === "صورة3" || key === "صورة2" || key === "صورة1" || key === "صورة السؤال" || key === "نص السؤال" || key === "رقم الخيار الصحيح" || key === "التفسير" || key === "التصنيف الفرعي" || key === "الدورة" || key === "قابل للخلط") {
                    continue
                }
                if (key === "الخيار الأول" && value === "null") {
                    answer = {
                        answer_content: element["التفسير"],
                        correctness: null,
                        answer_notes: null,
                        question_id: null,
                    }
                } else if (value !== "null") {
                    answer = {
                        answer_content: value,
                        correctness: key === "الخيار الأول" && element["رقم الخيار الصحيح"] === 0 ? true : key === "الخيار الثاني" && element["رقم الخيار الصحيح"] === 1 ? true : key === "الخيار الثالث" && element["رقم الخيار الصحيح"] === 2 ? true : key === "الخيار الرابع" && element["رقم الخيار الصحيح"] === 3 ? true : false,
                        answer_notes: null,
                        question_id: null,
                        url: key === "الخيار الأول" ? element["صورة1"] : key === "الخيار الثاني" ? element["صورة2"] : key === "الخيار الثالث" ? element["صورة3"] : key === "الخيار الرابع" ? element["صورة4"] : null
                    }
                } else {
                    continue
                }
                an.push(answer)
            }
            answers.push(an);
            questions.push(question);
        });
        const questionResults = await Question.bulkCreate(questions, { returning: true, transaction: transaction });

        for (let i = 0; i < questionResults.length; i++) {
            arrayOfSubSubjects.push(questions[i].sub_subject_ids.map(id => ({ 
                sub_subject_id: id, 
                question_id: questionResults[i].id 
            })));

            if (questions[i].previous_ids.length > 0) {
                arrayOfPrevious.push(questions[i].previous_ids.map(id => ({ 
                    previous_id: id, 
                    question_id: questionResults[i].id 
                })));
            }

            for (let j = 0; j < answers[i].length; j++) {
                answers[i][j].question_id = questionResults[i].id;
            }
        }
        const finalResult = answers.flat();
        const answerResults = await Answer.bulkCreate(finalResult, { transaction: transaction });
        const subSubjectQuestionResults = await Sub_subject_question.bulkCreate(arrayOfSubSubjects.flat(), {transaction: transaction});
        const previousQuestionResults = await Previous_question.bulkCreate(arrayOfPrevious.flat(), {transaction: transaction});
        await transaction.commit();
        fs.unlinkSync(path);
        // const ques = await Question.findAll({
        //     where: {
        //         url: { [Op.startsWith]: 'https://drive.google.com/file/', }
        //     }
        // })
        // for(let i = 0 ; i < ques.length ; i++){
        //     let temp = ques[i]
        //     temp.url = await update_image(temp)
        //     await temp.save()
        // }
        // const ans = await Answer.findAll({
        //     where: {
        //         url: { [Op.startsWith]: 'https://drive.google.com/file/', }
        //     }
        // })
        // for(let i = 0 ; i < ans.length ; i++){
        //     let temp = ans[i]
        //     temp.url = await update_image(temp)
        //     await temp.save()
        // }
        res.status(200).json({ message: "completed successfully" });
    } catch (error) {
        if (transaction) {
            await transaction.rollback();
        }
        res.status(500).json(error.message);
    }
}

//* for csv file
// exports.store = async (req, res, next) => {
//     const { path } = req.file;
//     let questions = [];
//     let answers = [];

//     try {
//         fs.createReadStream(path)
//             .pipe(csv())
//             .on('data', (row) => {
//                 let an = [];
//                 const question = {
//                     question_content: row["نص السؤال"],
//                     question_note: "",
//                     is_mcq: row["الخيار الأول"] !== "null" ? true : false,
//                     previous_id: row["الدورة"],
//                     sub_subject_id: row["التصنيف الفرعي"]
//                 };
//                 for (const [key, value] of Object.entries(row)) {
//                     let answer = {};
//                     if (key === "نص السؤال" || key === "رقم الخيار الصحيح" || key === "التفسير" || key === "التصنيف الفرعي" || key === "الدورة") {
//                         continue;
//                     }
//                     if (key === "الخيار الأول" && value === "null") {
//                         answer = {
//                             answer_content: row["التفسير"],
//                             correctness: null,
//                             answer_notes: null,
//                             question_id: null,
//                         };
//                     } else if (value !== "null") {
//                         answer = {
//                             answer_content: value,
//                             correctness: key === "الخيار الأول" && row["رقم الخيار الصحيح"] === '0' ? true : key === "الخيار الثاني" && row["رقم الخيار الصحيح"] === '1' ? true : key === "الخيار الثالث" && row["رقم الخيار الصحيح"] === '2' ? true : key === "الخيار الرابع" && row["رقم الخيار الصحيح"] === '3' ? true : false,
//                             answer_notes: null,
//                             question_id: null,
//                         };
//                     } else {
//                         continue;
//                     }
//                     an.push(answer);
//                 }
//                 answers.push(an);
//                 questions.push(question);
//             })
//             .on('end', async () => {
//                 const questionResults = await Question.bulkCreate(questions, { returning: true });
//                 for (let i = 0; i < questionResults.length; i++) {
//                     for (let j = 0; j < answers[i].length; j++) {
//                         answers[i][j].question_id = questionResults[i].id;
//                     }
//                 }
//                 const finalResult = answers.flat();
//                 const answerResults = await Answer.bulkCreate(finalResult);
//                 await fs.unlink(path);
//                 res.status(200).json(answerResults);
//             });
//     } catch (error) {
//         res.status(500).json(error.message);
//     }
// };

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    console.log(req.body);
    try {
        const question = await Question.findByPk(id);
        if (!question) {
            return res.status(404).json({ message: "no question found" })
        }
        question.question_content = req.body.question_content || question.question_content;
        question.question_notes = req.body.question_notes || question.question_notes;
        question.is_rollable = req.body.is_rollable;
        await question.save();
        if (req.body.sub_subject_ids) {
            await Sub_subject_question.destroy({
                where: {
                    question_id: question.id
                }
            });
            for (let i = 0; i < req.body.sub_subject_ids.length; i++) {
                const sub_subject_questions = await Sub_subject_question.create({
                    sub_subject_id: req.body.sub_subject_ids[i],
                    question_id: question.id
                });
            }
        }
        if (req.body.previous_ids) {
            await Previous_question.destroy({
                where: {
                    question_id: question.id
                }
            });
            for (let i = 0; i < req.body.previous_ids.length; i++) {
                const sub_subject_questions = await Previous_question.create({
                    previous_id: req.body.previous_ids[i],
                    question_id: question.id
                });
            }
        }
        for (let i = 0; i < req.body.answer.length; i++) {
            const answer = await Answer.update({
                answer_content: req.body.answer[i].answer_content,
                correctness: req.body.answer[i].correctness
            }, {
                where: {
                    id: req.body.answer[i].id
                }
            });
        }
        res.status(200).json({ message: 'question updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const questions = await Question.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'Questions deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.getQuestionsForSubSubject = async (req, res, next) => {
    try {
        const sub_subjects = await Sub_subject_question.findAll({
            attributes: ["question_id"],
            where: {
                sub_subject_id: req.params.sub_subject_id
            },
        });
        const questions = await Question.findAll({
            where: {
                id: sub_subjects.map((item) => ( item.question_id ))
            },
            order: [[Sequelize.literal('CASE WHEN sort IS NULL THEN 1 ELSE 0 END'), 'ASC'], ['sort', 'ASC']],
        });
        res.status(200).json(questions);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.sort = async (req, res, next) => {
    const { data } = req.body;
    try {
        for (let i = 0; i < data.length; i++) {
            const question = await Question.update({
                sort: i + 1
            }, {
                where: {
                    id: data[i].id
                }
            });
        }
        return res.status(200).json({ message: "completed successfully" })
    } catch (error) {
        res.status(500).json(error.message);
    }
}



//? to store questions
            // const answer = [
            //     { 
            //         answer_content: element["الخيار الأول"], 
            //         correctness: element["رقم الخيار الصحيح"] === 0 ? true : false,
            //         answer_notes: null,
            //         question_id: null,
            //     },
            //     { 
            //         answer_content: element["الخيار الثاني"], 
            //         correctness: element["رقم الخيار الصحيح"] === 1 ? true : false,
            //         answer_notes: null,
            //         question_id: null,
            //     },
            //     { 
            //         answer_content: element["الخيار الثالث"], 
            //         correctness: element["رقم الخيار الصحيح"] === 2 ? true : false,
            //         answer_notes: null,
            //         question_id: null,
            //     },
            //     { 
            //         answer_content: element["الخيار الرابع"], 
            //         correctness: element["رقم الخيار الصحيح"] === 3 ? true : false,
            //         answer_notes: null,
            //         question_id: null,
            //     }
            // ]
            // answers.push(answer)