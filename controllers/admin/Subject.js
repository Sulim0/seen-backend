const { Collage, University, Subject, Year, Bachelor } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const subjects = await Subject.findAll({
            include: [{
                model: Year,
                as: 'year',
                attributes: ['year_name'],
                include: [{
                    model: Collage,
                    as: 'collage',
                    attributes: ['collage_name', 'id'],
                    include: [{
                        model: University,
                        as: 'university',
                        attributes: ["university_name", "university_state", "id"]
                    }]
                }]
                
            },
            {
                model: Bachelor,
                as: 'bachelor',
                attributes: ['bachelor_name']
            }],
            raw: true
        });
        res.status(200).json(subjects);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { term, language, subject_name, year_id, subject_notes, bachelor_id } = req.body;
    try {
        const subject = await Subject.create({
            subject_name: subject_name,
            term: term,
            language: language,
            year_id: year_id,
            subject_notes: subject_notes,
            bachelor_id: bachelor_id
        });
        res.status(200).json(subject);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.storeSad = async (req, res, next) => {
    const { term, language, subject_name, subject_notes } = req.body;
    try {
        const year_id = await Year.findOne({
            where: {
                is_root: true,
                year_name: 'سادسة'
            }
        });
        if (!year_id) {
            return res.status(404).json({ message: 'select a root year'})
        }
        const subject = await Subject.create({
            subject_name: subject_name,
            term: term,
            language: language,
            year_id: year_id.id,
            subject_notes: subject_notes,
            bachelor_id: null
        });
        res.status(200).json(subject);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.storeTah = async (req, res, next) => {
    const { term, language, subject_name, subject_notes } = req.body;
    try {
        const year_id = await Year.findOne({
            attributes: ['id'],
            where: {
                is_root: true,
                year_name: 'تحضيرية'
            }
        });
        if (!year_id) {
            return res.status(404).json({ message: 'select a root year'})
        }
        const subject = await Subject.create({
            subject_name: subject_name,
            term: term,
            language: language,
            year_id: year_id.id,
            subject_notes: subject_notes,
            bachelor_id: null
        });
        res.status(200).json(subject);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const subject = await Subject.findByPk(id);
        if (!subject) {
            return res.status(404).json({ message: "no subject found"})
        }
        subject.subject_name = req.body.subject_name || subject.subject_name;
        subject.term = req.body.term || subject.term;
        subject.language = req.body.language || subject.language;
        subject.subject_notes = req.body.subject_notes || subject.subject_notes;
        subject.year_id = req.body.year_id;
        subject.bachelor_id = req.body.bachelor_id;
        await subject.save();
        res.status(200).json({ message: 'subject updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const subject = await Subject.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'subjects deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

