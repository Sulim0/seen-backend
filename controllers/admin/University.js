const { University } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const universities = await University.findAll();
        res.status(200).json(universities);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { university_name, university_state } = req.body;
    try {
        const universities = await University.create({
            university_name: university_name,
            university_state: university_state
        });
        res.status(200).json(universities);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const university = await University.findByPk(id);
        if (!university) {
            return res.status(404).json({ message: "no university found"})
        }
        university.university_name = req.body.university_name || university.university_name;
        university.university_state = req.body.university_state || university.university_state;
        await university.save();
        res.status(200).json({ message: 'university updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const university = await University.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'universities deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}