const { User, Student, Bachelorean, Year, Collage, Bachelor, Code, Subject, User_answer, Answer } = require('../../utils/models');
const { Op } = require('sequelize');
const getAllUsers = require('./helper/getAllUsers');
const getSpecificUsers = require('./helper/getSpecificUsers');

exports.index = async (req, res, next) => {
    try {
        return res.status(200).json(await getAllUsers());
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const students = await User.destroy({
            where: {
                id: ids
            }
        });
        return res.status(200).json({ message: "students deleted successfully" });
    } catch (error) {
        return res.status(500).json(error.message);
    }
};

exports.transfer = async (req, res, next) => {
    const { ids, year_id, bachelor_id } = req.body;
    console.log(req.body);
    try {
        if (year_id) {
            const student = await Student.update({
                year_id: year_id
            },
            {
                where: {
                    user_id: ids
                }
            });
            const user = await User.update({
                is_logged_in: false
            },
            {
                where: {
                    user_id: ids
                }
            });
            res.status(200).json({ message: 'student updated successfully' });
        } else {
            const bachelorean = await Bachelorean.update({
                bachelor_id: bachelor_id
            },
            {
                where: {
                    user_id: ids
                }
            });
            const user = await User.update({
                is_logged_in: false
            },
            {
                where: {
                    user_id: ids
                }
            });    
            res.status(200).json({ message: 'bachelorean updated successfully' });
        }
    } catch (error) {
        res.status(500).json(error.message);
    }
}


const count_wrong_answers_by_user = async (users, start_date, end_date) => {
    const result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const wrongs = await User_answer.count({
                where: {
                    user_id: users[i].id,
                    is_correct: false,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({ number_of_wrongs: wrongs })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_wrong_answers = async (users, start_date, end_date) => {
    let result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const wrongs = await User_answer.count({
                where: {
                    user_id: users[i].id,
                    is_correct: false,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            });
            result.push({ number_of_wrongs: wrongs })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_total_answers_by_user = async (users, start_date, end_date) => {
    const result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const answers = await User_answer.count({
                where: {
                    user_id: users[i].id,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({ number_of_answers: answers })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_total_answers = async (users, start_date, end_date) => {
    let result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const answers = await User_answer.count({
                where: {
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({ number_of_answers: answers })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_correct_answers_by_user = async (users, start_date, end_date) => {
    const result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const corrects = await User_answer.count({
                where: {
                    user_id: users[i].id,
                    is_correct: true,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({ number_of_corrects: corrects })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_correct_answers = async (users, start_date, end_date) => {
    let result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const corrects = await User_answer.count({
                where: {
                    user_id: users[i].id,
                    is_correct: true,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            });
            result.push({ number_of_corrects: corrects })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_codes_activated = async (users, start_date, end_date) => {
    let result = []
    try {
        for (let i = 0; i < users.length; i++) {
            const codes = await Code.count({
                where: {
                    user_id: users[i].id,
                    is_active: true,
                    createdAt: { [Op.between]: [start_date, end_date] }
                }
            })
            result.push({ number_of_codes: codes })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}

const count_codes_activated_by_user = async (users, start_date, end_date) => {
    const result = [];
    try {
        for (let i = 0; i < users.length; i++) {
            const codes = await Code.count({
                where: {
                    user_id: users[i].id,
                    createdAt: { [Op.between]: [start_date, end_date] }
                }
            })
            result.push({ number_of_codes: codes })
        }
        return result
    }
    catch (err) {
        console.log(err)
        return
    }
}


exports.statistics = async (req, res) => {
    try {
        const start_date = req.body.start_date
        const end_date = req.body.end_date
        console.log(req.body);
        if (req.body.user_ids.length > 0) {
            const ids = req.body.user_ids
            const users = await getSpecificUsers(ids)
            if (req.body.number_of_codes) {
                const codes = await count_codes_activated_by_user(users, start_date, end_date)
                for (let j = 0; j < codes.length; j++) {
                    users[j].number_of_codes = codes[j].number_of_codes
                }
            }
            if (req.body.number_of_wrongs) {
                const wrongs = await count_wrong_answers_by_user(users, start_date, end_date);
                for (let j = 0; j < wrongs.length; j++) {
                    users[j].number_of_wrongs = wrongs[j].number_of_wrongs
                }
            }
            if (req.body.number_of_corrects) {
                const corrects = await count_correct_answers_by_user(users, start_date, end_date)
                for (let j = 0; j < corrects.length; j++) {
                    users[j].number_of_corrects = corrects[j].number_of_corrects
                }
            }
            if (req.body.number_of_answers) {
                const answers = await count_total_answers_by_user(users, start_date, end_date)
                for (let j = 0; j < answers.length; j++) {
                    users[j].number_of_answers = answers[j].number_of_answers
                }
            }
            return res.json(users)
        }
        else {
            const users = await getAllUsers();
            if (req.body.number_of_codes) {
                const codes = await count_codes_activated(users, start_date, end_date);
                console.log(codes);
                for (let j = 0; j < codes.length; j++) {
                    users[j].number_of_codes = codes[j].number_of_codes;
                }
            }
            if (req.body.number_of_wrongs) {
                const wrongs = await count_wrong_answers(users, start_date, end_date)
                console.log(wrongs);
                for (let j = 0; j < wrongs.length; j++) {
                    users[j].number_of_wrongs = wrongs[j].number_of_wrongs;
                }
            }
            if (req.body.number_of_corrects) {
                const corrects = await count_correct_answers(users, start_date, end_date)
                for (let j = 0; j < corrects.length; j++) {
                    users[j].number_of_corrects = corrects[j].number_of_corrects;
                }
            }
            if (req.body.number_of_answers) {
                const answers = await count_total_answers(users, start_date, end_date)
                for (let j = 0; j < answers.length; j++) {
                    users[j].number_of_answers = answers[j].number_of_answers;
                }
            }
            return res.json(users);
        }
    }
    catch (err) {
        console.log(err)
        return res.json(err)
    }
}