const { Collage, University, Year } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const years = await Year.findAll({
            include: [{
                model: Collage,
                as: 'collage',
                attributes: ['collage_name'],
                include: [{
                    model: University,
                    as: "university",
                    attributes: ["university_name", "university_state", "id"]
                }]
            }],
            raw: true
        });
        res.status(200).json(years);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { collage_id, year_name } = req.body;
    try {
        const year = await Year.create({
            collage_id: collage_id,
            year_name: year_name
        });
        res.status(200).json(year);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.toggle = async (req, res, next) => {
    const { ids } = req.body;
    console.log(ids);
    try {
        const year = await Year.findOne({
            where: {
                id: ids[0],
                // is_root: true,
                year_name: ['تحضيرية', 'سادسة']
            }
        });
        if (!year) {
            return res.status(404).json({ message: 'Invalid year' });
        }
        if (year.year_name === 'تحضيرية') {
            const toggle = await Year.update({
                is_root: false
            }, {
                where: {
                    year_name: "تحضيرية"
                }
            });
            const newYear = await Year.update({
                is_root: true
            }, {
                where: {
                    id: ids[0]
                }
            });
            res.status(200).json(year);
        }
        if (year.year_name === 'سادسة') {
            const toggle = await Year.update({
                is_root: false
            }, {
                where: {
                    year_name: "سادسة"
                }
            });
            const newYear = await Year.update({
                is_root: true
            }, {
                where: {
                    id: ids[0]
                }
            });
            res.status(200).json(year);
        }
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const year = await Year.findByPk(id);
        if (!year) {
            return res.status(404).json({ message: "no year found"})
        }
        year.collage_id = req.body.collage_id || year.collage_id;
        year.year_name = req.body.year_name || year.year_name;
        await year.save();
        res.status(200).json({ message: 'year updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const year = await Year.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'years deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}