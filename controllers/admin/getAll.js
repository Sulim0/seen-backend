const { University, Collage, Year, Bachelor, Subject, Sub_subject } = require('../../utils/models');

exports.getAll = async (req, res, next) => {
    const obj = {};
    try {
        const universities = await University.findAll({
            attributes: ['university_name', "id"]
        });
        const collages = await Collage.findAll({
            attributes: ['collage_name', "id", "university_id"]
        });
        const years = await Year.findAll({
            attributes: ['year_name', "id", "collage_id"]
        });
        const bachelors = await Bachelor.findAll({
            attributes: ['bachelor_name', "id"]
        });
        const subjects = await Subject.findAll({
            attributes: ['subject_name', "id", "bachelor_id", "year_id"]
        });
        const sub_subjects = await Sub_subject.findAll({
            attributes: ['sub_subject_name', "id", "subject_id"],
            where: {
                father_id: null
            }
        });
        obj.universities = universities;
        obj.collages = collages;
        obj.years = years;
        obj.bachelors = bachelors
        obj.subjects = subjects
        obj.sub_subjects = sub_subjects
        return res.status(200).json(obj);
    } catch (error) {
        return res.status(500).json(error.message);
    }
}