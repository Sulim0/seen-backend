const { Question, Sub_subject, Previous, Sub_subject_question } = require('../../../utils/models');


const getAllQuestions = async () => {
    try {
        const questions = await Question.findAll({
            attributes: ["id", "question_content", "createdAt"],
            include: [{
                model: Sub_subject_question,
                as: "sub_subject_questions",
                attributes: ["id"],
                include: {
                    model: Sub_subject,
                    as: "sub_subject",
                    attributes: ["sub_subject_name"]
                }
            }, {
                model: Previous,
                as: "previous",
                attributes: ["previous_name"]
            }]
        });
        const data = questions.map((question) => ({
            ...question.toJSON(),
            sub_subject_questions: question?.sub_subject_questions.map((item) => item?.sub_subject?.sub_subject_name).join(' ,'),
            previous: question?.previous?.previous_name
        }));
        return data;
    } catch (error) {
        return [];
    }
}

module.exports = getAllQuestions