const { User, Student, Bachelorean, Year, Collage, Bachelor, Code, Subject, User_coupon, Coupon } = require('../../../utils/models');


const getSpecificUsers = async (ids) => {
    try {
        const users = await User.findAll({
            attributes: ['id', 'user_fullname', 'user_phonenumber'],
            include: [{
                model: Student,
                as: 'student',
                attributes: ['student_number'],
                include: [{
                    model: Year,
                    as: 'year',
                    attributes: ['year_name'],
                    include: [{
                        model: Collage,
                        as: 'collage',
                        attributes: ["collage_name"]
                    }]
                }]
            },
            {
                model: Bachelorean,
                as: 'bachelorean',
                attributes: ['state'],
                include: [{
                    model: Bachelor,
                    as: 'bachelor',
                    attributes: ['bachelor_name']
                }]
            },
            {
                model: Code,
                as: "codes",
                attributes: ["id", "code_content"],
                include: [{
                    model: Subject,
                    as: "subjects",
                    attributes: ["subject_name"],
                    through: { attributes: [] },
                }]
            },
            {
                model: User_coupon,
                as: 'user_coupon',
                attributes: ["id"],
                include: [{
                    model: Coupon,
                    as: 'coupon',
                    attributes: ["coupon_content"],
                }]
            }
            ],
            where: {
                id: ids
            }
        });
        const data = users.map((user) => ({
            id: user.id,
            username: user.user_fullname,
            phone: user.user_phonenumber,
            cardId: user.student ? user.student.student_number : "",
            year: user.student ? user.student.year.year_name : "",
            collage: user.student ? user.student.year.collage.collage_name : "",
            bachelor: user.bachelorean ? user.bachelorean.bachelor.bachelor_name : "",
            bachelorean: user.bachelorean ? user.bachelorean.state : "",
            subjects: user.codes ? user.codes.map((code) => code.subjects.map((subject) => subject.subject_name).join(', ')).join(', ') : "",
            codes: user.codes ? user.codes.map((code) => code.code_content).join(' ,') : "",
            coupons: user.user_coupon ? user.user_coupon.map((userCoupon) => userCoupon.coupon.coupon_content).join(', ') : "",
        }));
        return data
    } catch (error) {
        return []
    }
}

module.exports = getSpecificUsers