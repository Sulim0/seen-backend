const { Collage, University, Subject, Year, Bachelor, Sub_subject, Sequelize } = require('../../utils/models');

exports.index = async (req, res, next) => {
    try {
        const subSubjects = await Sub_subject.findAll({
            include: [{
                model: Subject,
                as: 'subject',
                attributes: ["subject_name"],
                include: [{
                    model: Year,
                    as: 'year',
                    attributes: ['year_name'],
                    include: [{
                        model: Collage,
                        as: 'collage',
                        attributes: ['collage_name', 'id'],
                        include: [{
                            model: University,
                            as: 'university',
                            attributes: ["university_name", "university_state", "id"]
                        }]
                    }]
                    
                },
                {
                    model: Bachelor,
                    as: 'bachelor',
                    attributes: ['bachelor_name']
                }],
            }],
            where: {
                father_id: null
            },
            raw: true
        });
        res.status(200).json(subSubjects);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.sub = async (req, res, next) => {
    try {
        const subSubjects = await Sub_subject.findAll({
            include: [{
                model: Subject,
                as: 'subject',
                attributes: ["subject_name"],
                include: [{
                    model: Year,
                    as: 'year',
                    attributes: ['year_name'],
                    include: [{
                        model: Collage,
                        as: 'collage',
                        attributes: ['collage_name', 'id'],
                        include: [{
                            model: University,
                            as: 'university',
                            attributes: ["university_name", "university_state", "id"]
                        }]
                    }]
                    
                },
                {
                    model: Bachelor,
                    as: 'bachelor',
                    attributes: ['bachelor_name']
                }],
            },
            {
                model: Sub_subject,
                as: "parent",
                attributes: ['sub_subject_name']
            }],
            where: {
                father_id: {
                    [Sequelize.Op.ne] : null
                }
            },
            raw: true
        });
        res.status(200).json(subSubjects);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.store = async (req, res, next) => {
    const { sub_subject_name, subject_id, sub_subject_notes, father_id } = req.body;
    try {
        const subSubject = await Sub_subject.create({
            sub_subject_name: sub_subject_name,
            subject_id: subject_id,
            sub_subject_notes: sub_subject_notes,
            father_id: father_id,
        });
        res.status(200).json(subSubject);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.edit = async (req, res, next) => {
    const { id } = req.params;
    try {
        const subSubject = await Sub_subject.findByPk(id);
        if (!subSubject) {
            return res.status(404).json({ message: "no subSubject found"})
        }
        subSubject.sub_subject_name = req.body.sub_subject_name || subSubject.sub_subject_name;
        subSubject.sub_subject_notes = req.body.sub_subject_notes || subSubject.sub_subject_notes;
        subSubject.subject_id = req.body.subject_id;
        subSubject.father_id = req.body.father_id;
        await subSubject.save();
        res.status(200).json({ message: 'Sub subject updated successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.destroy = async (req, res, next) => {
    const { ids } = req.body;
    try {
        const subSubjects = await Sub_subject.destroy({
            where: {
                id: ids
            }
        });
        res.status(200).json({ message: 'Sub subjects deleted successfully' });
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.getSubSubjectsForSubject = async (req, res, next) => {
    try {
        const subSubjects = await Sub_subject.findAll({
            where: {
                subject_id: req.params.subject_id,
                father_id: null
            },
            order: [['sort', 'ASC'], ['createdAt', 'ASC']]
        });
        res.status(200).json(subSubjects);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.getSubSubjectsForMainSubSubject = async (req, res, next) => {
    try {
        const subSubjects = await Sub_subject.findAll({
            where: {
                father_id: req.params.sub_subject_id
            },
            order: [['sort', 'ASC'], ['createdAt', 'ASC']]
        });
        res.status(200).json(subSubjects);
    } catch (error) {
        res.status(500).json(error.message);
    }
}

exports.sort = async (req, res, next) => {
    const { data } = req.body;
    try {
        for (let i = 0; i < data.length; i++) {
            data
            const subSubject = await Sub_subject.update({
                sort: i + 1
            }, {
                where: {
                    id: data[i].id
                }
            });
        }
        return res.status(200).json({ message: "completed successfully" })
    } catch (error) {
        res.status(500).json(error.message);
    }
}