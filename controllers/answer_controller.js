const {Answer , Question} = require('../utils/models')
const multer = require('multer')
const { Op } = require("sequelize");
const axios = require('axios');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/images/app')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
});
  
const upload = multer({ storage: storage })

const add_answer = async (req , res) => {
    try{
        let body = req.body
        let info = {
            answer_content: body.answer_content,
            answer_notes: body.answer_notes,
            correctness: body.correctness,
            question_id: req.params.id
        }
        const answer = await Answer.create(info)
        return res.json(answer)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const add_image_to_answer = async (req , res) => {
    try{
        const img = "https://localhost:8080/" + req.file.path;
        const id = req.params.id
        const answer = await Answer.findByPk(id)
        answer.url = img
        await answer.save();
        return res.json({message: 'done' , answer: answer})
    }
    catch(err){
        console.log(err)
    }
}

const add_multi_answers_one_question = async (req , res) => {
    try{
        const id = req.params.id
        const body = req.body
        const answers = []
        for(let i = 0 ; i < body.length ; i++){
            answers[i] = {
                answer_content: body[i].answer_content,
                answer_notes: body[i].answer_notes,
                correctness: body[i].correctness,
                question_id: id
            }
        }
        const result = await Answer.bulkCreate(answers)
        return res.json(result)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


const add_multi_answers_multi_questions = async (req , res) => {
    try{
        const body = req.body
        const answers = []
        for(let i = 0 ; i < body.length ; i++){
            answers[i] = {
                answer_content: body[i].answer_content,
                answer_notes: body[i].answer_notes,
                correctness: body[i].correctness,
                question_id: body[i].question_id
            }
        }
        const result = await Answer.bulkCreate(answers)
        return res.json(result)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const edit_answer = async (req , res) => {
    try{
        let id = req.params.id
        let answer = await Answer.findByPk(id)
        if(answer === null){return res.json({message: "This answer doesn't exist!"})}
        let info = {
            answer_content: body.answer_content || answer.answer_content,
            answer_notes: body.answer_notes || answer.answer_notes,
            correctness: body.correctness || answer.correctness,
            question_id: body.question_id || answer.question_id
        }
        answer = info
        await answer.save()
        return res.json({message: "done!"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const delete_answer = async (req , res) => {
    try{
        let id = req.params.id
        await Answer.destroy({where: {id: id}})
        return res.json({message: 'done!'})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_answers_of_question = async (req , res) => {
    try{
        const id = req.params.id
        const question = await Question.findByPk(id)
        if(question === null){return res.json({message: "This question doesn't exist!"})}
        const answers = Answer.findAll({
            where: {
                question_id: id
            }
        })
        return res.json(answers)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_answer = async (req , res) => {
    try{
        const id = req.params.id
        const answer = await Answer.findByPk(id)
        if(answer === null){return res.json({message: "This answer doesn't exist!"})}
        return res.json(answer)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const delete_answers = async (req , res) => {
    try{
        const body = req.body
        await Answer.destroy({where: {id: {[Sequelize.Op.in]: body}}})
        return res.json({message: "done!"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


const delete_answers_of_question = async (req , res) => {
    try{
        const id = req.params.id
        await Answer.destroy({where: {question_id: id}})
        return res.json({message: "done!"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const pwd = '/root/seen/public/images/app/seen-'
async function downloadImage(url, filename) {
    const response = await axios.get(url, { responseType: 'arraybuffer' });

    fs.writeFile(filename , response.data, (err) => {
      if (err) throw err;
      console.log('Image downloaded successfully!');
    });
  }

const update_image = async (obj) => {
    try{
      let input_img = obj.url;
      let n = input_img.split('/')
      let l = n.length
      let new_link =  "https://drive.usercontent.google.com/u/0/uc?id=" + n[l-2] + "&export=download"
      console.log(new_link)
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      const img_name = `/root/seen/public/images/app/seen-image-${uniqueSuffix}`
      await downloadImage(new_link, img_name);
      return "http://seen-sy.com:8080/public/images/app/seen-"+"image-"+uniqueSuffix
    }
    catch(err){
      console.log(err);
      return "";
    }
  }

  const url_update = async () => {
    try{
      const ques = await Question.findAll({
        where: {
            url: { [Op.startsWith]: 'https://drive.google.com/file/', }
        }
    })
    for(let i = 0 ; i < ques.length ; i++){
        let temp = ques[i]
        temp.url = await update_image(temp)
        await temp.save()
    }
    const ans = await Answer.findAll({
        where: {
            url: { [Op.startsWith]: 'https://drive.google.com/file/', }
        }
    })
    for(let i = 0 ; i < ans.length ; i++){
        let temp = ans[i]
        temp.url = await update_image(temp)
        await temp.save()
    }
    console.log("URLs Updated");
    console.log("//////////////////////");
    }
    catch(err){
      console.log(err)
    }
  }

module.exports = {
    get_answer,
    get_answers_of_question,
    add_answer,
    add_multi_answers_one_question,
    add_multi_answers_multi_questions,
    edit_answer,
    delete_answer,
    delete_answers,
    delete_answers_of_question,
    add_image_to_answer,
    url_update,
    upload
}
