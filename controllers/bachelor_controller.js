const {User  , Bachelor} = require('../utils/models')

const get_bachelors = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const bachelors = await Bachelor.findAll()
        return res.json(bachelors)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    get_bachelors
}