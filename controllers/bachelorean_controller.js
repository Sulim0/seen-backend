const {Bachelorean , Student , User , Sequelize} = require('../utils/models')

const add_bachelorean = async (req , res) => {
    try{
        let user_id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || checker.user_token!=token){return res.status(401).json({message : "Not Authorized!"})}
        // const user1 = await Student.findOne({where: {user_id: user_id}})
        // const user2 = await Bachelorean.findOne({where: {user_id: user_id}})
        // if(user1 || user2){return res.json({message: "this user is already registered"})}
        let bachelor_id = req.body.bachelor_id
        let state = req.body.state
        const [bachelorean , created] = await Bachelorean.findOrCreate({
            where: {
                user_id: user_id
            },
            defaults: {
                state: state ,
                bachelor_id: bachelor_id ,
                user_id: user_id
            }
        })
        checker.user_type = "bachelorean"
        await checker.save()
        bachelorean.bachelor_id = bachelor_id
        await bachelorean.save()
        console.log("done!")
        return res.json(bachelorean)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const delete_bachelorean = async (req , res) => {
    try{
        const id = req.params.id
        await Bachelorean.destroy({where: {id: id}})
        return res.json({message: "done!"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    add_bachelorean,
    delete_bachelorean
}
