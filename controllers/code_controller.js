const { Code, Subject_code, Subject, User, Sequelize } = require('../utils/models')
const datetime = require("date-and-time")

const scan_code = async (req, res) => {
    try {
        const code_content = req.body.code_content // get the code content from request
        const id = req.params.id // get user id
        const token = req.body.token
        const user = await User.findByPk(id)
        if (user.is_logged_in === false || token != user.user_token) { return res.status(401).json({ message: "Not Authorized!" }) }
        if (user === null) { return res.json({ message: "This user doesn't exist!" }) }
        const code = await Code.findOne({
            where: {
                code_content: code_content
            }
        }) // get the code from database
        if (code === null) { return res.json({ message: "code doesn't exist" }) }
        const flag = code.is_active
        console.log(code.date_of_activation)
        console.log(flag)
        if (code.date_of_activation === null && flag) { // check that the code is not taken and is active
            code.date_of_activation = Date.now()
            code.user_id = id
            const code_id = code.id
            await code.save()
            const subjects = await Subject_code.findAll(
                {
                    where: {
                        code_id: code_id
                    },
                    attributes: ['code_id'],
                    include: [
                        {
                            model: Subject,
                            as: 'subject',
                            attributes: ['id', 'subject_name']
                        }
                    ],
                    raw: true
                }
            )
            console.log("done!")
            return res.json({ code, subjects, message: "done" })
        }
        else {
            console.log("done!")
            return res.json({ message: "this code is not available" })
        }
    }
    catch (err) {
        console.log(err)
        return res.send(err)
    }
}

const get_code = async (req, res) => {
    try {
        const content = req.body.code_content
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if (checker.is_logged_in === false || token != checker.user_token) { return res.status(401).json({ message: "Not Authorized!" }) }
        const code = await Code.findOne({
            where: {
                code_content: content
            },
            include: [
                {
                    model: Subject_code,
                    as: 'subject_code',
                    attributes: ['code_id'],
                    include: [
                        {
                            model: Subject,
                            as: 'subject',
                            attributes: ['id', 'subject_name', 'subject_notes', 'term', 'language', 'bachelor_id', 'year_id']
                        }
                    ]
                }
            ]
        })
        return res.json(code)
    }
    catch (err) {
        console.log(err)
        return res.json(err)
    }
}

const getAllCodesForOneUserWithFinishedDate = async (req, res) => {
    try {
        const date = new Date();
        const { user_id } = req.body;
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if (checker.is_logged_in === false || token != checker.user_token) { return res.status(401).json({ message: "Not Authorized!" }) }
        const code = await Code.findAll({
            where: {
                user_id: user_id,
                is_active: true
            },
            include: [
                {
                    model: Subject,
                    as: 'subjects',
                    attributes: ['id', 'subject_name', 'subject_notes', 'term', 'language', 'bachelor_id', 'year_id']
                }
            ]
        });
        for (let i = 0; i < code.length; i++) {
            var now = new Date();
            var value = datetime.addDays(now, code[i].expiry_time);
            code[i].setDataValue('end_date', new Date(
                value
            ));
        }
        return res.json(code)
    }
    catch (err) {
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    scan_code,
    get_code,
    getAllCodesForOneUserWithFinishedDate
}