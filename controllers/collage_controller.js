const {Collage , Sequelize} = require('../utils/models')

const get_collages_of_university = async (req , res) => {
    try{
        let id = req.params.id
        const collages = await Collage.findAll(
            {
                where: {
                    university_id: id
                },
                attributes: ['id' , 'collage_name']
            }
        )
        console.log("done!")
        return res.json(collages)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_collage = async (req , res) => {
    try{
        const id = req.params.id
        const collage = await Collage.findByPk(id)
        return res.json(collage)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    get_collages_of_university,
    get_collage
}