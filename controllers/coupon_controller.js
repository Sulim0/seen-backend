const {Coupon , User_coupon , User , Subject_coupon , Subject , Sequelize} = require('../utils/models')
const datetime = require('date-and-time')

const apply_coupon = async (req , res) => {
    try{
        let id = req.params.id
        const user = await User.findByPk(id)
        if(user === null){return res.json({message: "This user doesn't exist!"})}
        const coupon_id = req.body.coupon_id
        const coupon = await Coupon.findByPk(coupon_id)
        if(coupon === null){return res.json({message: "This coupon doesn't exist!"})}
        const is_active = coupon.is_active
        const expiry_time = coupon.expiry_time
        if(!is_active){console.log("not active check"); return res.json({message: "This coupon is not active!"})}
        const limit = coupon.limit
        const user_coupon = await User_coupon.findOne({
            where: {
                user_id: id,
                coupon_id: coupon_id
            }
        })
        if(user_coupon != null){return res.json({message : "You already have this coupon applied!"})}
        const counter = await User_coupon.count({
            where: {
                coupon_id: coupon_id
            }
        })
        if(counter < limit){
            await User_coupon.create({
                user_id: id,
                coupon_id: coupon_id,
                is_expired: false,
                expiry_time: expiry_time
            })
            return res.json({coupon: coupon , message: "Done!"})
        }
        return res.json({message: "Limit exceeded!"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_coupon = async (req , res) => {
    try{
        const content = req.body.coupon_content.toLowerCase()
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        const coupon = await Coupon.findOne({
            where: {
                coupon_content: content
            },
            include: [
                {
                    model: Subject_coupon,
                    as: 'subject_coupon',
                    attributes: ['coupon_id'],
                    include: [
                        {
                            model: Subject,
                            as: 'subject',
                            attributes: ['id' , 'subject_name' , 'subject_notes' , 'term' , 'language' , 'year_id' , 'bachelor_id']
                        }
                    ],
                    raw: true
                }
            ]
        })
        if(coupon != null)return res.json(coupon)
        else{return res.json({message: "this coupon does not exist"})}
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const getAllCouponsForOneUserWithFinishedDate = async (req, res) => {
    try {
        const date = new Date();
        const { user_id } = req.body;
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if (checker.is_logged_in === false || token != checker.user_token) { return res.status(401).json({ message: "Not Authorized!" }) }
        const coupons_ids = await User_coupon.findAll(
            {
                where: {
                    user_id: user_id,
                    is_expired: false
                },
                order: ['coupon_id']
            }
        )
        const ids = []
        for(let i = 0 ; i < coupons_ids.length ; i++){
            ids.push(coupons_ids[i].coupon_id)
        }
        const coupons = await Coupon.findAll({
            where: {
                id: ids,
                is_active: true
            },
            include: [
                {
                    model: Subject,
                    as: 'subjects',
                    attributes: ['id', 'subject_name', 'subject_notes', 'term', 'language', 'bachelor_id', 'year_id']
                }
            ],
            order: ['id']
        });
        for (let i = 0; i < coupons.length; i++) {
            var now = new Date();
            var value = datetime.addDays(now, coupons_ids[i].expiry_time);
            coupons[i].setDataValue('end_date', new Date(
                value
            ));
            coupons[i].setDataValue('date_of_activation' , coupons_ids[i].createdAt);
        }
        return res.json(coupons)
    }
    catch (err) {
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    apply_coupon,
    get_coupon,
    getAllCouponsForOneUserWithFinishedDate
}