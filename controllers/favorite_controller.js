const {Favorite , User , Question , Sequelize} = require('../utils/models')

const add_to_favorite = async (req , res) => {
    try{
        const ids = req.body.ids
        const id = req.params.id
        const input = []
        for(let i = 0 ; i < ids.length ; i++){
            input.push({
                user_id: id,
                question_id : ids[i]
            })
        }
        const favorites = await Favorite.bulkCreate(input)
        return res.json(favorites)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const remove_favorites = async (req , res) => {
    try{
        const ids = req.body.ids
        const id = req.params.id
        const favorites = await Favorite.destroy({
            where : {
                user_id: id,
                question_id: {[Sequelize.Op.in]: ids}
            }
        })
        return res.json({message: "done!"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const sync_favorites = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const ids = req.body.ids
        const input = []
        for(let i = 0 ; i < ids.length ; i++){
            input.push({
                user_id: id,
                question_id : ids[i]
            })
        }
        const favorites = await Favorite.bulkCreate(input)
        return res.json(favorites)
    }
    catch(err){
        console.log(err);
        return res.json(err);
    }
}

const get_favorites_of_user = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const favorites = await Favorite.findAll({
            where: {
                user_id: id
            },
            include: [
                {
                    model: Question,
                    as: 'question',
                    attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'is_rollable' , 'url']
                }
            ]
        })
        return res.json(favorites)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    add_to_favorite,
    remove_favorites,
    sync_favorites,
    get_favorites_of_user
}