const {Link , User} = require('../utils/models')

const get_links = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const links = await Link.findAll()
        return res.json(links)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    get_links
}