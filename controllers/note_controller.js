const {Note , Question , User} = require('../utils/models')

const add_note =  async (req , res) => {
    try{
        const {user_id , question_id , token , note} = req.body
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not  authorized!"})}
        const [notem , created] = await Note.findOrCreate({
            where: {
                user_id: user_id,
                question_id: question_id
            },
            defaults:{
                user_id: user_id,
                question_id: question_id,
                note: note
            }
        })
        if(created){return res.json(notem)}
        else{
            notem.note = note
            await notem.save()
            return res.json(notem)
        }
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const sync_notes = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const notes = req.body.notes
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        await Note.destroy({
            where: {
                user_id : id
            }
        })
        const new_notes = []
        for(let i = 0 ; i < notes.length ; i++){
            new_notes.push({
                question_id: notes[i].question_id,
                note: notes[i].note,
                user_id: id
            })
        }
        await Note.bulkCreate(new_notes)
        return res.json({message: 'done!'})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    add_note,
    sync_notes
}