const {Previous , Question , Answer , Previous_question} = require('../utils/models')

const get_previous = async (req , res) => {
    try{
        const id = req.params.id
        const previous = await Previous.findByPk(id , {
            include: [
                {
                    model: Previous_question,
                    as: 'previous_questions',
                    attributes: ['id' , 'previous_id' , 'question_id'],
                    include: [
                        {
                            model: Question,
                            as: 'question',
                            attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'url' , 'is_rollable'],
                            include: [
                                {
                                    model: Answer,
                                    as: 'answer',
                                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                                }
                            ]
                        }
                    ]
                }
            ]
        })
        if(previous === null){return res.json({message: "This previous doesn't exist!"})}
        return res.json(previous)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_previouses_of_subject = async (req , res) => {
    try{
        const id = req.params.id
        const previouses = await Previous.findAll({
            where: {
                subject_id: id
            },
            include: [
                {
                    model: Previous_question,
                    as: 'previous_questions',
                    attributes: ['id' , 'previous_id' , 'question_id'],
                    include: [
                        {
                            model: Question,
                            as: 'question',
                            attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'is_rollable' , 'url'],
                            include: [
                                {
                                    model: Answer,
                                    as: 'answer',
                                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                                }
                            ]
                        }
                    ]
                }
            ]
        })
        return res.json(previouses)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

// const get_previouses_of_user = async (req , res) => {
//     try{
//         const id = req.params.id

//     }
//     catch(err){
//         console.log(err)
//         return res.json(err)
//     }
// }

module.exports = {
    get_previous,
    get_previouses_of_subject
}