const {Question , Answer , User_answer , User , Sequelize , Note , Sub_subject_question , Previous_question} = require('../utils/models')
const multer = require('multer')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/images/app')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
});
  
const upload = multer({ storage: storage })

const getAllQuestions = require('./admin/helper/getAllQuestions')
const add_question = async (req , res) => {
    try{
        const body = req.body
        let info = {
            question_content: body.question_content,
            question_notes: body.question_notes,
            is_mcq: body.is_mcq,
            previous_id: body.previous_id,
            sub_subject_id: body.sub_subject_id
        }
        const sub_subject_id = info.sub_subject_id
        const question =  await Question.create(info)
        const asos = []
        for (let i = 0; i < sub_subject_id.length; i++) {
            asos.push({"sub_subject_id" : sub_subject_id[i] , "question_id" : question.id})
        }
        const pres = []
        for(let i = 0 ; i < previous_id ; i++){
            pres.push({"previous_id" : previous_id[i] , "question_id" : question.id})
        }
        const preses = await Previous_question.bulkCreate(pres)
        const asoses  = await Sub_subject_question.bulkCreate(asos)
        return res.json(question)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const add_image_to_question = async (req , res) => {
    try{
        const img = "https://localhost:8080/" + req.file.path;
        const id = req.params.id
        const question = await Question.findByPk(id)
        question.url = img
        await question.save();
        return res.json({message: 'done' , question: question})
    }
    catch(err){
        console.log(err)
    }
}

const get_question = async (req , res) => {
    try{
        let id = req.params.id
        const question = await Question.findByPk(id , {
            include: {
                model: Answer,
                as: 'answer',
                attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'is_mcq' , 'is_rollable' , 'url']
            }
        })
        console.log('done!')
        return res.json(question)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


const get_questions_of_sub_subject = async (req , res) => {
    try{
        let id = req.params.id
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token!=checker.user_token){return res.status(401).json("Not Authorized!")}
        const questions_ids = await Sub_subject_question.findAll({
            where: {
                sub_subject_id: id
            }
        })
        const ids = [] 
        for (let i = 0; i < questions_ids.length; i++) {
            ids.push(questions_ids[i].question_id)
        }
        const questions = await Question.findAll({
            where: {
                id: ids
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                },
                {
                    model: Note,
                    as: 'notes',
                    required: false
                }
            ]
        })
        console.log('done!')
        return res.json(questions)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


const get_questions_of_previous = async (req , res) => {
    try{
        let id = req.params.id
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token!=checker.user_token){return res.status(401).json("Not Authorized!")}
        const questions_ids = await Previous_question.findAll({
            where: {
                previous_id: id
            }
        })
        const ids = [] 
        for (let i = 0; i < questions_ids.length; i++) {
            ids.push(questions_ids[i].question_id)
        }
        const questions = await Question.findAll({
            where: {
                id: ids
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                },
                {
                    model: Note,
                    as: 'notes',
                    required: false
                }
            ]
        })
        console.log('done!')
        return res.json(questions)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const question_statistics = async (req , res) => {
    try{
        let result = []
        let answers = []
        const questions = await getAllQuestions()
        for(let i = 0 ; i < questions.length ; i++){
            let counts = [
            {type: "a_count" , count: 0} ,
            {type: "b_count" , count: 0} , 
            {type: "c_count" , count: 0} ,
            {type: "d_count" , count: 0} ,
            {type: "e_count" , count: 0} , 
            {type: "corrects" , count: 0},
            {type: "wrongs" , count: 0}]
            answers = await Answer.findAll({
                where: {
                    question_id : questions[i].id
                }
            })
            for(let j = 0 ; j < answers.length ; j++){
                if(answers[j].id === null){continue}
                else{                      
                    counts[j].count = await User_answer.count({
                        where: {
                            answer_id : answers[j].id
                        }
                    })
                    if(answers[j].correctness){
                        counts[5].count = counts[j].count
                    }
                    else{
                        counts[6].count += counts[j].count
                    }
                    answers[j] = {
                        answer: answers[j],
                        counts: {
                            count: counts[j].count,
                            corrects: counts[5].count,
                            wrongs: counts[6].count
                        }
                    }
                }
            }
            result.push({
                question: questions[i],
                stats: answers
            })
        }
        return res.json(result)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const question_statistics_of_user = async (req , res) => {
    try{
        const user_id = req.params.user_id
        let result = []
        let answers = []
        const questions = await getAllQuestions()
        for(let i = 0 ; i < questions.length ; i++){
            let counts = [
            {type: "a_count" , count: 0} ,
            {type: "b_count" , count: 0} , 
            {type: "c_count" , count: 0} ,
            {type: "d_count" , count: 0} ,
            {type: "e_count" , count: 0} , 
            {type: "corrects" , count: 0},
            {type: "wrongs" , count: 0}]
            answers = await Answer.findAll({
                where: {
                    question_id : questions[i].id
                }
            })
            for(let j = 0 ; j < answers.length ; j++){
                if(answers[j].id === null){continue}
                else{
                    counts[j].count = await User_answer.count({
                        where: {
                            answer_id : answers[j].id,
                            user_id: user_id
                        }
                    })
                    if(answers[j].correctness){
                        counts[5].count = counts[j].count
                    }
                    else{
                        counts[6].count += counts[j].count
                    }
                    answers[j] = {
                        answer: answers[j],
                        counts: {
                            count: counts[j].count,
                            corrects: counts[5].count,
                            wrongs: counts[6].count
                        }
                    }
                }
            }
            result.push({
                question: questions[i],
                stats: answers
            })
        }
        return res.json(result)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    add_question,
    get_questions_of_sub_subject,
    get_question,
    get_questions_of_previous,
    question_statistics,
    question_statistics_of_user,
    add_image_to_question,
    upload
}