const db = require('../utils/models')


const {Student , User , Year , Bachelorean} = require('../utils/models')

// Funcs

const add_student = async (req, res) => {
    try{
        let user_id = req.params.id 
        let student_number = req.body.student_number
        let year_id = req.body.year_id
        let token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || checker.user_token!=token){return res.status(401).json({message : "Not Authorized!"})}
        const [student, created] = await Student.findOrCreate({
            where: {
                user_id: user_id
            },
            defaults: {
                student_number: student_number ,
                user_id: user_id ,
                year_id: year_id
            }
        })
        checker.user_type = "student"
        await checker.save()
        student.year_id = year_id
        await student.save()
        let subject_year_id = year_id
        let year = await Year.findByPk(subject_year_id)
        if(year.year_name === "السادسة" && year.is_root === false){
            year = await Year.findOne({
                where: {
                    year_name: "السادسة",
                    is_root: true
                }
            })
            subject_year_id = year.id
        }
        if(year.year_name === "تحضيرية" && year.is_root === false){
            year = await Year.findOne({
                where: {
                    year_name: "تحضيرية",
                    is_root: true
                }
            })
            subject_year_id = year.id
        }
        var res_student = student
        res_student.setDataValue('subject_year_id' , subject_year_id);
        console.log("done!")
        return res.status(200).json(res_student)
    }
    catch (err){
        console.log(err)
        return res.json(err)
    }
}


const update_student = async (req , res) => {
    try{
        let id = req.params.id
        let student_number = req.body.student_number
        let year_id = req.body.year_id
        const student = await Student.update({student_number: student_number , year_id: year_id} , {where : {id : id}})
        console.log("done!")
        return res.status(200).json({message: "done"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


const delete_student = async (req , res) => {
    try{
        let id = req.params.id
        await Student.destroy({where:
            {id: id}
        })
        console.log("done!")
        return res.status(200).json({message: "done"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


module.exports = {
    add_student,
    update_student,
    delete_student
}