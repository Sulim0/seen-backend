const {Sub_subject , User} = require('../utils/models')

const get_subs_of_subject = async (req , res) => {
    try{
        let id = req.params.id
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        const subs = await Sub_subject.findAll(
            {
                where: {
                    subject_id: id
                },
                attributes: ['id' , 'sub_subject_name' , 'sub_subject_notes' , 'father_id' , 'subject_id']
            }
        )
        console.log("done!")
        return res.json(subs)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    get_subs_of_subject
}