const {Subject , User , Code , Coupon , Subject_coupon , Subject_code , Year , User_coupon} = require('../utils/models')

const get_subject = async (req , res) => {
    try{
        const id = req.params.id
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Auhtorized!"})}
        const codes = await Code.findAll({
            where: {
                user_id: user_id
            }
        })
        const codes_ids = []
        for(let i = 0 ; i<codes.length ; i++){
            codes_ids.push(codes[i].id)
        }
        const coupons = await User_coupon.findAll({
            where: {
                user_id: user_id
            }
        })
        const coupons_ids = []
        for(let i = 0 ; i < coupons.length ; i++){
            coupons_ids.push(coupons[i].coupon_id)
        }
        const subject = await Subject.findByPk(id , {
            include: [
                {
                    model: Code,
                    as: 'codes',
                    required: false,
                    attributes: ['id' , 'code_content' , 'code_notes' , 'code_name' , 'expiry_time' , 'is_active' , 'date_of_activation' , 'type' , 'user_id'],
                    where: {
                        id: codes_ids,
                        is_active: true
                    },
                },
                {
                    model: Coupon,
                    as: 'coupons',
                    required: false,
                    attributes: ['id' , 'coupon_content' , 'coupon_notes' , 'is_active' , 'expiry_time'],
                    where: {
                        id: coupons_ids,
                        is_active: true
                    }
                }
            ]
        })
        return res.json(subject)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_subjects_of_year = async (req , res) => {
    try{
        const user_id = req.body.user_id
        const token = req.body.token
        let year_id = req.body.year_id
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        let year = await Year.findByPk(year_id)
        if(year.year_name === "السادسة" && year.is_root === false){
            year = await Year.findOne({
                where: {
                    year_name: "السادسة",
                    is_root: true
                }
            })
            year_id = year.id
        }
        if(year.year_name === "تحضيرية" && year.is_root === false){
            year = await Year.findOne({
                where: {
                    year_name: "تحضيرية",
                    is_root: true
                }
            })
            year_id = year.id
        }
        const subjects = await Subject.findAll({
            where: {
                year_id: year_id
            }
        })
        return res.json(subjects)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_subjects_of_bachelor = async (req , res) => {
    try{
        const user_id = req.body.user_id
        const token = req.body.token
        const bachelor_id = req.body.bachelor_id
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        const subjects = await Subject.findAll({
            where: {
                bachelor_id: bachelor_id
            }
        })
        return res.json(subjects)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    get_subject,
    get_subjects_of_year,
    get_subjects_of_bachelor
}