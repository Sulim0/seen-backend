const {University , User} = require('../utils/models')

const get_university = async (req , res) => {
    try{ 
        const id = req.params.id
        const university = await University.findByPk(id)
        return res.json(university)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


const get_universities = async (req , res) => {
    try{ 
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const university = await University.findAll()
        return res.json(university)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


module.exports = {
    get_university,
    get_universities
}