const { where, Op } = require('sequelize')
const question_router = require('../routes/question_router')
const {User , Answer , Question , Sub_subject_question , Sub_subject , Code , User_answer , Sequelize} = require('../utils/models')

const submit_answers = async (req , res) => {
    try{
        const id = req.params.id
        const answers = req.body.answers
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        const input = []
        for(let i = 0 ; i < answers.length ; i++){
            input.push({
                user_id: id,
                answer_id: answers[i].id,
                is_correct: answers[i].correctness
            })
        }
        const user_answers = await User_answer.bulkCreate(input)
        console.log("done!")
        return res.json(user_answers)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_answers = async (req , res) => {
    try{
        const id = req.params.id
        const answers = await User_answer.findAll({
            where: {
                user_id : id
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id'],
                    include: [
                        {
                            model: Question,
                            as: 'question',
                            attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'is_rollable' , 'url'],
                            include: [
                                {
                                    model: Answer,
                                    as: 'answer',
                                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                                }
                            ]
                        }
                    ]
                }
            ]
        })
        console.log("done!")
        return res.json(answers)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_wrong_answers = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message : "Not Authorized!"})}
        const answers = await User_answer.findAll({
            where: {
                user_id : id,
                is_correct: false
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'question_id' , 'url'],
                    include: [
                        {
                            model: Question,
                            as: 'question',
                            attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'url' , 'is_rollable'],
                            include: [
                                {
                                    model: Answer,
                                    as: 'answer',
                                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                                },
                                {
                                    model: Sub_subject_question,
                                    as: 'sub_subject_questions',
                                    attributes: ['question_id'],
                                    include: [
                                        {
                                            model: Sub_subject,
                                            as: 'sub_subject',
                                            attributes: ['subject_id']
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            attributes: ["id"]
        })
        console.log("done!")
        return res.json(answers)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_wrong_answers_of_sub_subject = async (req , res) => {
    try{
        const id = req.params.id
        const sub_id = req.body.sub_subject_id
        const questions_ids = await Sub_subject_question.findAll({
            where: {
                sub_subject_id: sub_id
            }
        })
        const ids = [] 
        for (let i = 0; i < questions_ids.length; i++) {
            ids.push(questions_ids[i].question_id)
        }
        const answers = await User_answer.findAll({
            where: {
                user_id : id,
                is_correct: false
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'question_id' , 'url'],
                    include: [
                        {
                            model: Question,
                            as: 'question',
                            attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'is_rollable' , 'url'],
                            where: {
                                id : {[Sequelize.Op.in]: ids}
                            },
                            include: [
                                {
                                    model: Answer,
                                    as: 'answer',
                                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                                }
                            ]
                        }
                    ]
                }
            ],
            attributes: ["id"]
        })
        console.log("done!")
        return res.json(answers)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


// const max_wronged_answer_by_user = async (id) => {
//     try{
//         const User_answer.max
//     }
//     catch(err){

//     }
// }


const count_wrong_answers_by_user = async (ids , start_date , end_date) => {
    try{
        const result = []
        let id = 0
        for(let i = 0 ; i < ids.length ; i++){
            id = ids[i]
            const wrongs = await User_answer.count({
                where: {
                    user_id: id,
                    is_correct: false,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({id: id , number_of_wrongs : wrongs})
        }
        return result
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_wrong_answers = async (start_date , end_date) => {
    try{
        const wrongs = await User_answer.count({
            where: {
                is_correct: false,
                createdAt: { [Op.between]: [start_date, end_date] }
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id']
                }
            ]
        })
        return wrongs
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_total_answers_by_user = async (ids , start_date , end_date) => {
    try{
        const result = []
        let id  = 0
        for(let i = 0 ; i < ids.length ; i++){
            id = ids[i]
            const answers = await User_answer.count({
                where: {
                    user_id: id,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({id: id , number_of_answers : answers})
        }
        return result
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_total_answers = async (start_date , end_date) => {
    try{
        const answers = await User_answer.count({
            where: {
                createdAt: { [Op.between]: [start_date, end_date] }
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id']
                }
            ]
        })
        return answers
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_correct_answers_by_user = async (ids , start_date , end_date) => {
    try{
        const result = []
        let id = 0
        for(let i = 0 ; i < ids.length ; i++){
            id = ids[i]
            const corrects = await User_answer.count({
                where: {
                    user_id: id,
                    is_correct: true,
                    createdAt: { [Op.between]: [start_date, end_date] }
                },
                include: [
                    {
                        model: Answer,
                        as: 'answer',
                        attributes: ['id']
                    }
                ]
            })
            result.push({id: id , number_of_corrects : corrects})
        }
        return result
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_correct_answers = async (start_date , end_date) => {
    try{
        const corrects = await User_answer.count({
            where: {
                is_correct: true,
                createdAt: { [Op.between]: [start_date, end_date] }
            },
            include: [
                {
                    model: Answer,
                    as: 'answer',
                    attributes: ['id']
                }
            ]
        })
        return corrects
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_codes_activated = async (start_date , end_date) => {
    try{
        const codes = await Code.count({
            where:{
                user_id: {[Op.ne] : null},
                is_active : true,
                date_of_activation: { [Op.between]: [start_date, end_date] }
            }
        })
        return codes
    }
    catch(err){
        console.log(err)
        return 
    }
}

const count_codes_activated_by_user = async (ids , start_date , end_date) => {
    try{
        const result = []
        let id = 0
        for(let i = 0 ; i < ids.length ; i++){
            id = ids[i]
            const codes = await Code.count({
                where:{
                    user_id: id,
                    date_of_activation: { [Op.between]: [start_date, end_date] }
                }
            })
            result.push({id : id , number_of_codes : codes})
        }
        return result
    }
    catch(err){
        console.log(err)
        return 
    }
}

const statistics = async (req , res) => {
    try{
        const start_date = req.body.start_date
        const end_date = req.body.end_date
        const wanted_info = req.body.wanted_info
        let result = []
        if(req.body.user_ids != null){
            const ids = req.body.user_ids
            for(let i = 0 ; i < ids.length ; i++){
                result.push({id : ids[i]})
            }
            for(let i = 0 ; i < wanted_info.length ; i++){
                if(wanted_info[i] === "number_of_codes"){
                    const codes = await count_codes_activated_by_user(ids , start_date , end_date)
                    for(let j = 0 ; j < codes.length ; j++){
                        result[j].number_of_codes = codes[j].number_of_codes
                    }
                }
                if(wanted_info[i] === "number_of_wrongs"){
                    const wrongs = await count_wrong_answers_by_user(ids , start_date , end_date)
                    for(let j  = 0 ; j < wrongs.length ; j++){
                        result[j].number_of_wrongs = wrongs[j].number_of_wrongs
                    }
                }
                if(wanted_info[i] === "number_of_corrects"){
                    const corrects = await count_correct_answers_by_user(ids , start_date , end_date)
                    for(let j = 0 ; j < corrects.length ; j++){
                        result[j].number_of_corrects = corrects[j].number_of_corrects
                    }
                }
                if(wanted_info[i] === "number_of_answers"){
                    const answers = await count_total_answers_by_user(ids , start_date , end_date)
                    for(let j = 0 ; j < answers.length ; j++){
                        result[j].number_of_answers = answers[j].number_of_answers
                    }
                }
            }
            return res.json(result)
        }
        else{
            for(let i = 0 ; i < wanted_info.length ; i++){
                if(wanted_info[i] === "number_of_codes"){
                    const codes = await count_codes_activated(start_date , end_date)
                    for(let j = 0 ; j < codes.length ; j++){
                        result[j].number_of_codes = codes[j].number_of_codes
                    }
                }
                if(wanted_info[i] === "number_of_wrongs"){
                    const wrongs = await count_wrong_answers(start_date , end_date)
                    for(let j  = 0 ; j < wrongs.length ; j++){
                        result[j].number_of_wrongs = wrongs[j].number_of_wrongs
                    }
                }
                if(wanted_info[i] === "number_of_corrects"){
                    const corrects = await count_correct_answers(start_date , end_date)
                    for(let j = 0 ; j < corrects.length ; j++){
                        result[j].number_of_corrects = corrects[j].number_of_corrects
                    }
                }
                if(wanted_info[i] === "number_of_answers"){
                    const answers = await count_total_answers(start_date , end_date)
                    for(let j = 0 ; j < answers.length ; j++){
                        result[j].number_of_answers = answers[j].number_of_answers
                    }
                }
            }
            return res.json(result)
        }
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


module.exports = {
    submit_answers,
    get_user_answers,
    get_user_wrong_answers,
    get_user_wrong_answers_of_sub_subject,
    statistics
}