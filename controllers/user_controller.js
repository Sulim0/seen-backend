const db = require('../utils/models')
const path = require('path')
const body_parser = require('body-parser')
const jwt = require("jsonwebtoken");
require('dotenv').config();
const axios = require('axios')
const { Op } = require("sequelize");
const {User , Code , Year , Subject , Question , Answer , Note , Sub_subject , Sub_subject_question , Subject_code , Subject_coupon , Collage , University , Bachelor , Bachelorean , Student , Coupon , User_coupon , Previous , Previous_question , Sequelize} = require('../utils/models');
const subject_code_model = require('../utils/models/subject_code_model');
const bachelorean_router = require('../routes/bachelorean_router');
const { where } = require('sequelize');
const multer = require('multer')
const fs = require('fs');
const util = require('util');

const readFile1 = util.promisify(fs.readFile);

const getChapter = async (req , res) => {
    try {
        const id = req.params.id
        const token = req.body.token
        const version = req.body.version
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const read_data = await readFile1('versions.json', 'utf8');
        const info = JSON.parse(read_data);
        if(info.current_version != version){return res.status(402).json({message: "this is not the latest version"})}
        fs.readFile('chapterInfo.json', 'utf8', function (err, data) {
            if (err) throw err;
            const info = JSON.parse(data);
            return res.status(200).json({
                chapter: info.chapter
            });
        });
    }
    catch (error) {
        console.log(error)
        return res.status(500).json({ error });
    }
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/images/app')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
  })
  
const upload = multer({ storage: storage })

// replace with http://seen-sy.com:8080/
const add_image = async (req , res , next) => {
    try{
        const img = "https://534f-31-9-172-247.ngrok-free.app/" + req.file.path;
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        checker.user_image = img
        await checker.save()
        return res.json({message: 'done' , user_image: img})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, './uploads');
//       },
//     filename: function (req, file, cb) {
//         cb(null, Date.now() + file.originalname.toString().split(":").pop());
//     }
// });

// const upload_img = multer({storage: storage });


const user_login = async (req, res, next) => {
    try {
        const access_token = req.body.access_token;
        var resp;
        await axios.request({
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' +access_token
            },
            method: "GET",
            url: `https://www.googleapis.com/oauth2/v3/userinfo`
        }).then(response => {
            resp = response.data;
        }).catch(erro => {
            return res.status(401).json({message: "Not Authorized!"})
        });
        let user_username = resp.email.split("@")[0];
 //       if(resp.email_verified){
        const [user, created] = await User.findOrCreate({
            where: { userSubId: resp.sub },
            attributes: ['id' , 'user_fullname' , 'user_username' , 'user_email' , 'user_type' , 'user_phonenumber' , 'sex' , 'user_image' , 'user_token' , 'is_logged_in'],
            include: [
                {
                    model: Student,
                    as: 'student',
                    required: false,
                    attributes: ['id' , 'student_number' , 'year_id'],
                    include: [
                        {
                            model: Year,
                            as: 'year',
                            attributes: ['id' , 'year_name' , 'collage_id'],
                            include: [
                                {
                                    model: Collage,
                                    as: 'collage',
                                    attributes: ['id' , 'collage_name' , 'university_id'],
                                    include: [
                                        {
                                            model: University,
                                            as: 'university',
                                            attributes: ['id' , 'university_name' , 'university_state']
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    model: Bachelorean,
                    as: 'bachelorean',
                    required: false,
                    attributes: ['id' , 'state' , 'bachelor_id'],
                    include: [
                        {
                            model: Bachelor,
                            as: 'bachelor',
                            attributes: ['id' , 'bachelor_name']
                        }
                    ]
                }
            ],
            defaults: {
              user_fullname: resp.name , 
              user_email: resp.email , 
              user_username: user_username,
              user_type: "none" , 
              is_logged_in: true,
              user_image: resp.picture
            }
        });
        const token = jwt.sign({ id: user.id.toString(), g_id: resp.sub, locale: resp.locale, user_email: user.user_email, user_username: user_username , updatedAt: user.updatedAt}, process.env.LOGIN_KEY);
        user.user_token = token
        user.is_logged_in = true
        await user.save()
        let subject_year_id = null
        if(!created){
            if(user.user_type === "student"){
                let stu = await Student.findOne({
                    where: {
                        user_id: user.id
                    }
                })
                subject_year_id = stu.year_id
                let year = await Year.findByPk(subject_year_id)
                if(year.year_name === "السادسة" && year.is_root === false){
                    year = await Year.findOne({
                        where: {
                            year_name: "السادسة",
                            is_root: true
                        }
                    })
                    subject_year_id = year.id
                }
                if(year.year_name === "تحضيرية" && year.is_root === false){
                    year = await Year.findOne({
                        where: {
                            year_name: "تحضيرية",
                            is_root: true
                        }
                    })
                    subject_year_id = year.id
                }
            }
        }
        var res_user = user
        res_user.setDataValue('subject_year_id' , subject_year_id);
        return res.json(res_user)
        // }
        // else{
        //     return res.status(401).json({message: "Not Authorized!"})
        // }
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
};


const user_sign_in = async (req , res) => {
    try{
        const {user_username , user_password} = req.body;
        const user = await User.findOne({
            where: { 
                user_username: user_username         
            },
            attributes: ['id' , 'user_fullname' , 'user_username' , 'user_password' , 'user_email' , 'user_type' , 'user_phonenumber' , 'sex' , 'user_image' , 'user_token' , 'is_logged_in'],
            include: [
                {
                    model: Student,
                    as: 'student',
                    required: false,
                    attributes: ['id' , 'student_number' , 'year_id'],
                    include: [
                        {
                            model: Year,
                            as: 'year',
                            attributes: ['id' , 'year_name' , 'collage_id'],
                            include: [
                                {
                                    model: Collage,
                                    as: 'collage',
                                    attributes: ['id' , 'collage_name' , 'university_id'],
                                    include: [
                                        {
                                            model: University,
                                            as: 'university',
                                            attributes: ['id' , 'university_name' , 'university_state']
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    model: Bachelorean,
                    as: 'bachelorean',
                    required: false,
                    attributes: ['id' , 'state' , 'bachelor_id'],
                    include: [
                        {
                            model: Bachelor,
                            as: 'bachelor',
                            attributes: ['id' , 'bachelor_name']
                        }
                    ]
                }
            ]
        });
        if(user === null){return res.json({message: "wrong username"})}
        if(user.user_password != user_password){return res.json({message: "wrong password"})}
        const token = jwt.sign({ id: user.id.toString(), user_fullname: user.user_fullname , user_username: user.user_username , user_phonenumber: user.user_phonenumber , g_id: user.userSubId, user_email: user.user_email, updatedAt: user.updatedAt}, process.env.LOGIN_KEY);
        user.user_token = token
        user.is_logged_in = true
        await user.save()
        let subject_year_id = null
        if(user.user_type === "student"){
            let stu = await Student.findOne({
                where: {
                    user_id: user.id
                }
            })
            subject_year_id = stu.year_id
            let year = await Year.findByPk(subject_year_id)
            if(year.year_name === "السادسة" && year.is_root === false){
                year = await Year.findOne({
                    where: {
                        year_name: "السادسة",
                        is_root: true
                    }
                })
                subject_year_id = year.id
            }
            if(year.year_name === "تحضيرية" && year.is_root === false){
                year = await Year.findOne({
                    where: {
                        year_name: "تحضيرية",
                        is_root: true
                    }
                })
                subject_year_id = year.id
            }
        }
        var res_user = user
        res_user.setDataValue('subject_year_id' , subject_year_id);
        delete res_user.user_password;
        return res.json(res_user)
    }
    catch(err){
        console.log(err);
        return res.json(err);
    }
}


const user_sign_up = async (req , res) => {
    try{
        const {user_phonenumber , user_username , user_password , user_fullname , sex} = req.body
        const userSubId = (new Date()).getTime().toString(36) + Math.random().toString(36).slice(2)  
        const checker = await User.findOne({
            where: {
                user_username: user_username
            }
        });
        if(checker != null){return res.json({message: "this username is not available"})}      
        const user = await User.create({
            user_fullname: user_fullname,
            user_password: user_password,
            user_username: user_username,
            user_phonenumber: user_phonenumber,
            user_type: 'none',
            is_logged_in: true,
            sex: sex,
            userSubId: userSubId
        });
        const token = jwt.sign({ id: user.id.toString(), user_fullname: user.user_fullname , user_username: user.user_username , user_phonenumber: user.user_phonenumber , g_id: user.userSubId, user_email: user.user_email, updatedAt: user.updatedAt}, process.env.LOGIN_KEY);
        user.user_token = token
        user.userSubId = user.id.toString() + '-' + user.userSubId
        await user.save();
        const res_user = await User.findByPk(user.id , {
            attributes: ['id' , 'user_fullname' , 'user_email' , 'user_username' , 'user_type' , 'user_phonenumber' , 'sex' , 'user_image' , 'user_token'],
        });
        return res.json(res_user);
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}



const create_account = async (req , res) => {
    try{
        const id = req.params.id
        const {token , sex , phone} = req.body
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || checker.user_token!=token){return res.status(401).json({message : "Not Authorized!"})}
        else{
            checker.sex = sex
            checker.user_phonenumber = phone
            await checker.save()
            return res.json(checker)
        }
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}


// user create account

// const add_user = async (req, res) => {
//     try{
//         let body = req.body;
//             var data = {
//             user_fullname: body.user_fullname,
//             user_password: body.user_password,
//             user_email: body.user_email,
//             user_phonenumber: body.user_phonenumber,
//             user_type: body.user_type,
//             is_logged_in: false
//             }

//         const user = await User.create(data)
//         console.log(user)
//         console.log("done!")
//         return res.status(200).json(user)
//     }
//     catch (err){
//         console.log(err)
//         return res.json(err)
//     }
// }


const update_user = async (req , res) => {
    try{
        let id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message : "Not Authorized!"})}
        let user_username = req.body.user_username
        const username_checker = await User.findOne({
            where: {
                user_username: user_username,
                id: { [Op.ne]: id }
            }
        })
        if(username_checker != null){return res.json({message : "this username is not available"})}
        let body = req.body
        let data = {
            user_fullname: body.user_fullname,
            user_username: user_username,
            // user_email: body.user_email,
            user_phonenumber: body.user_phonenumber,
            // user_type: body.user_type,
            sex: body.sex
        }
        await User.update(data , {where : {id : id}})
        console.log("done!")
        return res.status(200).json({message: "done"})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const delete_user = async (req , res) => {
    try{
        let id = req.params.id
        await User.destroy({where:
            {id: id}
        })
        console.log("done!")
        return res.status(200).json({result : 1})
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_subjects = async (req , res) => {
    try{
        let id = req.params.id //get the id of the year
        const {user_id , token} = req.body
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || checker.user_token != token){return res.status(401).json({message: "Not Authorized!"})}
        const year = await Year.findByPk(id) 
        const collage_id = year.collage_id //get the id of the collage this year belongs to
        const years = await Year.findAll({
            attributes: ['id'],
            where: {
                collage_id: collage_id
            }
        }) // get all years of this collage
        const years_ids = [];
        for(let i = 0 ; i<years.length ; i++){
            years_ids.push(years[i].id)
        } // convert years into years_ids
        const subjects = await Subject.findAll({
            include: [
                {model: Year,
                as: 'year',
                attributes: ['year_name'],
                include: [{
                    model: Collage,
                    as: 'collage',
                    attributes: ['collage_name']
                }]}
            ],
            where: {
                year_id: {[Sequelize.Op.in]: years_ids}
            },
            raw: true
        }) //get all subjects of this collage (incuding year name and collage name for each one)
        console.log("done!")
        return res.json(subjects)
    }
    catch(err){
        console.log(err)
        return res.send(err)
    }
}

const get_my_active_subjects = async (req , res) => {
    try{
        let id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token!=checker.user_token){return res.status(401).json({message : "Not Authorized!"})}
        let ids = []
        const subjects_ids = []
        console.log('///////////////////////////////////////////////////////')
        const codes = await Code.findAll({
            where: {
                user_id: id
            }
        })
        for(let i = 0 ; i < codes.length ; i++){
            ids.push(codes[i].id)
        }
        const sub_codes = await Subject_code.findAll({
            where: {
                code_id : ids
            }
        })
        ids = []
        for(let i = 0 ; i < sub_codes.length ; i++){
            subjects_ids.push(sub_codes[i].subject_id)
        }
        const coupons = await User_coupon.findAll({
            where: {
                user_id: id
            }
        })
        for(let i = 0 ; i < coupons.length ; i++){
            ids.push(coupons[i].coupon_id)
        }
        const sub_coupons = await Subject_coupon.findAll({
            where: {
                coupon_id: ids
            }
        })
        for(let i = 0 ; i < sub_coupons.length ; i++){
            subjects_ids.push(sub_coupons[i].subject_id)
        }
        const subjects = await Subject.findAll({
            where: {
                id: subjects_ids
            },
            include: [
                {
                    model: Code,
                    as: 'codes',
                    required: false,
                    where: {
                        user_id: id
                    }
                },
                {
                    model: Coupon,
                    as: 'coupons',
                    required: false,
                    where: {
                        id: ids
                    }
                }
            ]
        })
        console.log("////////////////////////////ffffffffffffffffffffff//////////////////////////")
        return res.json(subjects)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

// const get_my_active_sub_subjects = async (req , res) => {
//     try{

//     }
//     catch(err){

//     }
// }

const get_my_subjects = async (req , res) => {
    try{
        let id = req.params.id
        let subjects = []
        const user_id = req.body.user_id
        const token = req.body.token
        const checker = await User.findByPk(user_id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message : "Not Authorized!"})}
        if(checker.user_type === "student"){
            const stu = await Student.findOne(
                {
                    where: {
                        user_id: id
                    }
                }
            )
            let year_id = stu.year_id
            let year = await Year.findByPk(year_id)
            if(year.year_name === "السادسة" && year.is_root === false){
                year = await Year.findOne({
                    where: {
                        year_name: "السادسة",
                        is_root: true
                    }
                })
                year_id = year.id
            }
            if(year.year_name === "تحضيرية" && year.is_root === false){
                year = await Year.findOne({
                    where: {
                        year_name: "تحضيرية",
                        is_root: true
                    }
                })
                year_id = year.id
            }
            const codes_ids = await Code.findAll(
                {
                    where: {
                        user_id: id,
                        is_active: true
                    }
                }
            )
            const ids1 = []
            for(let i = 0; i<codes_ids.length ; i++){
                ids1.push(codes_ids[i].id)
            }
            const coupon_ids = await User_coupon.findAll(
                {
                    where: {
                        user_id: id,
                        is_expired: false
                    }
                }
            )
            const ids2 = []
            for(let i = 0 ; i<coupon_ids.length ; i++){
                ids2.push(coupon_ids[i].coupon_id)
            }
            subjects = await Subject.findAll(
                {
                    where:{
                        year_id: year_id
                    },
                    include: [
                        {
                            model: Coupon,
                            as: 'coupons',
                            required: false,
                            attributes: ['id' , 'coupon_content' , 'coupon_notes' , 'is_active' , 'expiry_time'],
                            where:{
                                id: ids2
                            }
                        },
                        {
                            model: Code,
                            as: 'codes',
                            required: false,
                            attributes: ['id' , 'code_content' , 'code_notes' , 'expiry_time' , 'date_of_activation' , 'is_active' , 'agent' , 'type'],
                            where:{
                                id: ids1
                            }
                        }
                    ]
                }
            )
        }
        if(checker.user_type === "bachelorean"){
            const bac = await Bachelorean.findOne({
                where: {
                    user_id: id
                }
            })
            const bac_id = bac.bachelor_id
            const codes_ids = await Code.findAll(
                {
                    where: {
                        user_id: id,
                        is_active: true
                    }
                }
            )
            const ids1 = []
            for(let i = 0; i<codes_ids.length ; i++){
                ids1.push(codes_ids[i].id)
            }
            const coupon_ids = await User_coupon.findAll(
                {
                    where: {
                        user_id: id,
                        is_expired: false
                    }
                }
            )
            const ids2 = []
            for(let i = 0 ; i<coupon_ids.length ; i++){
                ids2.push(coupon_ids[i].coupon_id)
            }
            subjects = await Subject.findAll(
                {
                    where:{
                        bachelor_id: bac_id
                    },
                    include: [
                        {
                            model: Coupon,
                            as: 'coupons',
                            required: false,
                            attributes: ['id' , 'coupon_content' , 'coupon_notes' , 'is_active' , 'expiry_time'],
                            where:{
                                id: ids2
                            }
                        },
                        {
                            model: Code,
                            as: 'codes',
                            required: false,
                            attributes: ['id' , 'code_content' , 'code_notes' , 'expiry_time' , 'date_of_activation' , 'is_active' , 'agent' , 'type'],
                            where:{
                                id: ids1
                            }
                        }
                    ]
                }
            )
        }        
        return res.json(subjects)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_subjects_questions = async (req , res) => {
    try{
        let id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token!=checker.user_token){return res.status(401).json({message : "Not Authorized!"})}
        const subjects_ids = []
        const codes_ids = await Code.findAll(
            {
                where: {
                    user_id: id,
                    is_active: true
                }
            }
        ) /// get user codes
        const ids1 = []
        for(let i = 0; i<codes_ids.length ; i++){
            ids1.push(codes_ids[i].id)
        }
        const subjects1 = await Subject_code.findAll(
            {
                where: {
                    code_id : ids1
                }
            }
        )
        for (let i = 0; i < subjects1.length; i++) {
            subjects_ids.push(subjects1[i].subject_id)            
        }
        const coupon_ids = await User_coupon.findAll(
            {
                where: {
                    user_id: id
                }
            }
        )
        const ids2 = []
        for(let i = 0 ; i<coupon_ids.length ; i++){
            ids2.push(coupon_ids[i].coupon_id)
        }
        const subjects2 = await Subject_coupon.findAll(
            {
                where: {
                    coupon_id: ids2
                }
            }
        )
        for (let i = 0; i < subjects2.length; i++) {
            subjects_ids.push(subjects2[i].subject_id)            
        }
        const subs_ids = await Sub_subject.findAll({
            where: {
                subject_id: subjects_ids
            }
        })
        const final_ids = []
        for (let i = 0; i < subs_ids.length; i++) {
            final_ids.push(subs_ids[i].id)            
        }
        const questions = await Sub_subject_question.findAll(
            {
                where: {
                    sub_subject_id : final_ids
                },
                include: [
                    {
                        model: Question,
                        as: 'question',
                        attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'url'],
                        include: [
                            {
                                model: Answer,
                                as: 'answer',
                                attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                            },
                            {
                                model: Note,
                                as: 'notes',
                                required: false
                            }
                        ]
                    }
                ]
            }
        )
        return res.json(questions)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_previouses = async (req , res) => {
    try{
        const id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token != checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        let ids = []
        const subjects_ids = []
        const codes = await Code.findAll({
            where: {
                user_id: id
            }
        })
        for(let i = 0 ; i < codes.length ; i++){
            ids.push(codes[i].id)
        }
        const sub_codes = await Subject_code.findAll({
            where: {
                code_id : ids
            }
        })
        ids = []
        for(let i = 0 ; i < sub_codes.length ; i++){
            subjects_ids.push(sub_codes[i].subject_id)
        }
        const coupons = await User_coupon.findAll({
            where: {
                user_id: id
            }
        })
        for(let i = 0 ; i < coupons.length ; i++){
            ids.push(coupons[i].coupon_id)
        }
        const sub_coupons = await Subject_coupon.findAll({
            where: {
                coupon_id: ids
            }
        })
        for(let i = 0 ; i < sub_coupons.length ; i++){
            subjects_ids.push(sub_coupons[i].subject_id)
        }
        const previouses = await Previous.findAll({
            where: {
                subject_id: subjects_ids
            },
            include: [
                {
                    model: Previous_question,
                    as: 'previous_question',
                    attributes: ['id'],
                    include: [
                        {
                            model: Question,
                            as: 'question',
                            attributes: ['id' , 'question_content' , 'question_notes' , 'is_mcq' , 'url'],
                            include: [
                                {
                                    model: Answer,
                                    as: 'answer',
                                    attributes: ['id' , 'answer_content' , 'answer_notes' , 'correctness' , 'url']
                                },
                                {
                                    model: Note,
                                    as: 'notes',
                                    required: false
                                }
                            ]
                        }
                    ]
                }
            ],
            order: ['sort' , 'subject_id']
        })
        return res.json(previouses)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

const get_user_sub_subjects = async (req , res) => {
    try{
        let id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token!=checker.user_token){return res.status(401).json({message : "Not Authorized!"})}
        const subjects_ids = []
        var year_id = null
        var bachelor_id = null
        if(checker.user_type==="student"){
            year_id = await Student.findOne(
                {
                    where: {
                        user_id: id
                    }
                }
            ) /// get user
            year_id = year_id.year_id
            let year = await Year.findByPk(year_id)
            if(year!= null && year.year_name === "السادسة" && year.is_root === false){
                year = await Year.findOne({
                    where: {
                        year_name: "السادسة",
                        is_root: true
                    }
                })
                year_id = year.id
            }
            if(year!= null && year.year_name === "تحضيرية" && year.is_root === false){
                year = await Year.findOne({
                    where: {
                        year_name: "تحضيرية",
                        is_root: true
                    }
                })
                year_id = year.id
            }
        }
        else{ /// get year of user
         bachelor_id = await Bachelorean.findOne(
            {
                where: {
                    user_id : id
                }
            }
        )
        bachelor_id = bachelor_id.bachelor_id}
        const codes_ids = await Code.findAll(
            {
                where: {
                    user_id: id
                }
            }
        ) /// get user codes
        const ids1 = []
        for(let i = 0; i<codes_ids.length ; i++){
            ids1.push(codes_ids[i].id)
        }
        const subjects1 = await Subject.findAll(
            {
                where:{
                    [Op.or]: [{ year_id: year_id }, { bachelor_id: bachelor_id }]
                },
                include: [
                    {
                        model: Subject_code,
                        as: 'subject_code',
                        attributes: ['subject_id'],
                        include: [
                            {
                            model: Code,
                            as: 'code',
                            attributes: ['id'],
                            where:{
                                id: {[Sequelize.Op.in]: ids1}
                            }
                            }
                        ]
                    }
                ]
            }
        )
        for (let i = 0; i < subjects1.length; i++) {
            subjects_ids.push(subjects1[i].id)            
        }
        const coupon_ids = await User_coupon.findAll(
            {
                where: {
                    user_id: id
                }
            }
        )
        const ids2 = []
        for(let i = 0 ; i<coupon_ids.length ; i++){
            ids2.push(coupon_ids[i].coupon_id)
        }
        const subjects2 = await Subject.findAll(
            {
                where:{
                    [Op.or]: [{ year_id: year_id }, { bachelor_id: bachelor_id }]
                },
                include: [
                    {
                        model: Subject_coupon,
                        as: 'subject_coupon',
                        attributes: ['subject_id'],
                        include: [
                            {
                            model: Coupon,
                            as: 'coupon',
                            attributes: ['id'],
                            where:{
                                id: {[Sequelize.Op.in]: ids2}
                            }
                            }
                        ]
                    }
                ]
            }
        )
        for (let i = 0; i < subjects2.length; i++) {
            subjects_ids.push(subjects2[i].id)            
        }
        const subs_ids = await Sub_subject.findAll({
            where: {
                subject_id: subjects_ids
            }
        })
        const final_ids = []
        for (let i = 0; i < subs_ids.length; i++) {
            final_ids.push(subs_ids[i].id)            
        }
        const subs = await Sub_subject.findAll(
            {
                where: {
                    id : final_ids
                },
                attributes: ['id' , 'sub_subject_name' , 'sub_subject_notes' , 'sort' , 'subject_id' , 'father_id'],
                order: ['sort'],
                group: ['subject_id' , 'father_id' , 'sub_subject_name' , 'sub_subject_notes' , 'sort' , 'id']
            }
        )
        return res.json(subs)
    }
    catch(err){
        console.log(err)
        return res.json(err)
  
    }
}

const get_user_codes = async (req , res) => {
    try{
        let id = req.params.id
        const token = req.body.token
        const checker = await User.findByPk(id)
        if(checker.is_logged_in === false || token!=checker.user_token){return res.status(401).json({message: "Not Authorized!"})}
        const codes = await Code.findAll({
            where: {
                user_id: id
            }
        })
        console.log("done!")
        return res.json(codes)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    delete_user,
    update_user,
    get_my_subjects,
    get_my_active_subjects,
    get_user_subjects,
    get_user_codes,
    get_user_subjects_questions,
    user_login,
    create_account,
    get_user_sub_subjects,
    get_user_previouses,
    getChapter,
    add_image,
    upload,
    user_sign_in,
    user_sign_up
}