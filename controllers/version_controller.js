const fs = require('fs');
const fse = require('fs-extra');
const util = require('util');

const readFile1 = util.promisify(fs.readFile);

module.exports = {
    get_version: (req, res) => {
        try {
            fs.readFile('versions.json', 'utf8', function (err, data) {
                if (err) throw err;
                const info = JSON.parse(data);
                return res.status(200).json({
                    version: info.current_version
                });
            });
        }
        catch (error) {
            return res.status(500).json({ error });
        }
    },
    update_version: async (req, res) => {
        try {
            const new_version = req.body.new_version
            let last_version = "";
            let previouses = "";
            const read_data = await readFile1('versions.json', 'utf8');
            const info = JSON.parse(read_data);
            
            last_version = info.current_version
            previouses = info.previous_versions
            const data = {
                current_version: new_version,
                previous_versions: `${previouses}-${last_version}`
            } 
            fse.writeJson('versions.json', data, { flag: 'w' }, function (err) {
                if (err) throw err;
                return res.status(200).json({ message: "Success" });
            });
        }
        catch (error) {
            console.log(error)
            return res.status(500).json(error);
        }
    }
}