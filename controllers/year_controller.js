const {Year , Sequelize} = require('../utils/models')

const get_years_of_collage = async (req , res) => {
    try{
        let id = req.params.id
        const years = await Year.findAll(
            {
                where: {
                    collage_id: id
                },
                attributes: ['id' , 'year_name']
            }
        )
        console.log("done!")
        return res.status(200).json(years)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
}

module.exports = {
    get_years_of_collage
}