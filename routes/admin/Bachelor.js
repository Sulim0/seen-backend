const express = require('express');
const router = express.Router();
const bachelorController = require('../../controllers/admin/Bachelor');

router.get('/', bachelorController.index);

router.post('/', bachelorController.store);

router.put('/:id', bachelorController.edit);

router.delete('/', bachelorController.destroy);

module.exports = router;