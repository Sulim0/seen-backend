const express = require('express');
const router = express.Router();
const chapterController = require('../../controllers/admin/ChapterInfo');

router.get('/', chapterController.getChapter);

router.post('/', chapterController.setChapter);

module.exports = router;