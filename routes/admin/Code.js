const express = require('express');
const router = express.Router();
const codeController = require('../../controllers/admin/Code');

router.get('/', codeController.index);

router.get('/:id', codeController.get);

router.post('/', codeController.store);

router.put('/:id', codeController.edit);

router.delete('/', codeController.destroy);

router.put('/', codeController.updateActive);

module.exports = router;