const express = require('express');
const router = express.Router();
const collageController = require('../../controllers/admin/Collage');

router.get('/', collageController.index);

router.post('/', collageController.store);

router.put('/:id', collageController.edit);

router.delete('/', collageController.destroy);

module.exports = router;