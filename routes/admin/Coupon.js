const express = require('express');
const router = express.Router();
const couponController = require('../../controllers/admin/Coupon');

router.get('/', couponController.index);

router.post('/', couponController.store);

router.put('/:id', couponController.edit);

router.delete('/', couponController.destroy);

router.put('/', couponController.updateActive);

module.exports = router;