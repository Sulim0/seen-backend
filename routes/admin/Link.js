const express = require('express');
const router = express.Router();
const linkController = require('../../controllers/admin/Link');

router.get('/', linkController.index);

router.post('/', linkController.store);

router.put('/:id', linkController.edit);

router.delete('/', linkController.destroy);

module.exports = router;