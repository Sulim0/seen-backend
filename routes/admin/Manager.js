const express = require('express');
const router = express.Router();
const managerController = require('../../controllers/admin/Manager');

router.post('/register', managerController.register);

router.post('/login', managerController.login);

router.put('/verify/:manager_id', managerController.verify);

module.exports = router;