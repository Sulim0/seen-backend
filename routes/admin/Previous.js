const express = require('express');
const router = express.Router();
const previousController = require('../../controllers/admin/Previous');

router.put('/sort', previousController.sort);

router.get('/', previousController.index);

router.post('/', previousController.store);

router.put('/:id', previousController.edit);

router.delete('/', previousController.destroy);

router.get('/:subject_id', previousController.getCoursesForSubject);


module.exports = router;