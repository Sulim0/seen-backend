const express = require('express');
const multer = require('multer');
const router = express.Router();
const questionController = require('../../controllers/admin/Question');
const statisticController = require('../../controllers/question_controller');

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/admin');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname);
    }
});
const fileFilter = (req, file, cb) => {
    cb(null, true);
};

const upload = multer({ storage: fileStorage, fileFilter: fileFilter });

router.get('/', questionController.index);

router.post('/', upload.single('file'), questionController.store);

router.put('/:id', questionController.edit);

router.delete('/', questionController.destroy);

router.get('/statistics', statisticController.question_statistics);

router.get('/statistics/:user_id', statisticController.question_statistics_of_user);

module.exports = router;