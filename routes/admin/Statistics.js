const statistics_controller = require('../../controllers/admin/User')

const statistics_router = require('express').Router()

statistics_router.post('/user-info' , statistics_controller.statistics)


module.exports = statistics_router