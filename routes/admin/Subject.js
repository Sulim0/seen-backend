const express = require('express');
const router = express.Router();
const subjectController = require('../../controllers/admin/Subject');

router.post('/sad', subjectController.storeSad);

router.get('/', subjectController.index);

router.post('/', subjectController.store);

router.post('/tah', subjectController.storeTah);


router.put('/:id', subjectController.edit);

router.delete('/', subjectController.destroy);

module.exports = router;