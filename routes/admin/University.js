const express = require('express');
const router = express.Router();
const universityController = require('../../controllers/admin/University');

router.get('/', universityController.index);

router.post('/', universityController.store);

router.put('/:id', universityController.edit);

router.delete('/', universityController.destroy);

module.exports = router;