const express = require('express');
const router = express.Router();
const userController = require('../../controllers/admin/User');

router.get('/', userController.index);

router.delete('/', userController.destroy);

router.put('/', userController.transfer);

module.exports = router;