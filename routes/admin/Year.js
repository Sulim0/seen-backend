const express = require('express');
const router = express.Router();
const yearController = require('../../controllers/admin/Year');

router.get('/', yearController.index);

router.post('/', yearController.store);

router.put('/toggle', yearController.toggle);

router.put('/:id', yearController.edit);

router.delete('/', yearController.destroy);

module.exports = router;