const router = require('express').Router();
const isAuth = require('../../middleware/isAuth')

router.post('/auth', require('../../middleware/isAuthMddleware'));

router.use('/', require('./Manager'));

router.use(isAuth)

router.use('/collages', require('./Collage'));

router.use('/universities', require('./University'));

router.use('/subjects', require('./Subject'));

router.use('/years', require('./Year'));

router.use('/bachelors', require('./Bachelor'));

router.use('/previous', require('./Previous'));

router.use('/sub-subjects', require('./subSubject'));

router.use('/get-all', require('../../controllers/admin/getAll').getAll);


router.use('/codes', require('./Code'));

router.use('/coupons', require('./Coupon'));

router.use('/users', require('./User'));

router.use('/questions', require('./Question'));

router.use('/links', require('./Link'));

router.use('/statistics', require('./Statistics'));


router.use('/chapter', require('./ChapterInfo'));

module.exports = router;