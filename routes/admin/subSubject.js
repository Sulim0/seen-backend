const express = require('express');
const router = express.Router();
const subSubjectController = require('../../controllers/admin/subSubject');

router.get('/', subSubjectController.index);

router.get('/sub', subSubjectController.sub);

router.post('/', subSubjectController.store);

router.put('/sort', subSubjectController.sort);

router.put('/:id', subSubjectController.edit);

router.delete('/', subSubjectController.destroy);

router.get('/:subject_id', subSubjectController.getSubSubjectsForSubject);

router.get('/sub/:sub_subject_id', subSubjectController.getSubSubjectsForMainSubSubject);

module.exports = router;