const answer_controller = require('../controllers/answer_controller')

const answer_router = require('express').Router()

// create
answer_router.post('/add-answer/:id' , answer_controller.add_answer)
answer_router.post('/add-multi-answers-one-question/:id' , answer_controller.add_multi_answers_one_question)
answer_router.post('/add-multi-answers-multi-questions' , answer_controller.add_multi_answers_multi_questions)

// read
answer_router.get('/get-answers-of-question/:id' , answer_controller.get_answers_of_question)
answer_router.get('/get-answer/:id' , answer_controller.get_answer)

// update
answer_router.patch('/edit-answer/:id' , answer_controller.edit_answer)
answer_router.post('/add-image/:id' , answer_controller.upload.single('image') , answer_controller.add_image_to_answer)

// delete
answer_router.delete('/delete-answer/:id' , answer_controller.delete_answer)
answer_router.delete('/multi-delete' , answer_controller.delete_answers)
answer_router.delete('/delete-answers-of-question' , answer_controller.delete_answers_of_question)


module.exports = answer_router