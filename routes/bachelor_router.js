const bachelor_controller = require('../controllers/bachelor_controller')

const bachelor_router = require('express').Router()

bachelor_router.put('/get-bachelors/:id' , bachelor_controller.get_bachelors)

module.exports = bachelor_router