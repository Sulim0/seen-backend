const bachelorean_controller = require('../controllers/bachelorean_controller')

const bachelorean_router = require('express').Router()


// create
bachelorean_router.post('/add-bachelorean/:id' , bachelorean_controller.add_bachelorean)

// delete
bachelorean_router.delete('/delete/:id' , bachelorean_controller.delete_bachelorean)


module.exports = bachelorean_router