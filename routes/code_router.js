const code_controller = require('../controllers/code_controller')

const code_router = require('express').Router()


//APIs

code_router.put('/my-code' , code_controller.getAllCodesForOneUserWithFinishedDate)
code_router.put('/scan/:id' , code_controller.scan_code)
code_router.put('/get-code' , code_controller.get_code)
//

module.exports = code_router