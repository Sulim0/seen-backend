const collage_controller = require('../controllers/collage_controller')

const collage_router = require('express').Router()

collage_router.get('/get-collages-of-university/:id' , collage_controller.get_collages_of_university)
collage_router.get('/get-collage/:id' , collage_controller.get_collage)

module.exports = collage_router