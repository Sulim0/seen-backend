const coupon_controller = require('../controllers/coupon_controller')

const coupon_router = require('express').Router()

coupon_router.put('/apply-coupon/:id' , coupon_controller.apply_coupon)

coupon_router.put('/get-coupon' , coupon_controller.get_coupon)

coupon_router.put('/my-coupon' , coupon_controller.getAllCouponsForOneUserWithFinishedDate)


module.exports = coupon_router