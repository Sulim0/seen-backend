const favorite_controller = require('../controllers/favorite_controller')

const favorite_router = require('express').Router()

favorite_router.post('/add-favorites/:id' , favorite_controller.add_to_favorite)

favorite_router.delete('/remove-favorites/:id' , favorite_controller.remove_favorites)

favorite_router.post('/sync-favorites/:id' , favorite_controller.sync_favorites)

favorite_router.put('/get-favorites-of-user/:id' , favorite_controller.get_favorites_of_user)

module.exports = favorite_router