const link_controller = require('../controllers/link_controller')

const link_router = require('express').Router()

link_router.put('/:id' , link_controller.get_links)

module.exports = link_router