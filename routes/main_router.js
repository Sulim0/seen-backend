const main_router = require('express').Router();

main_router.use('/user', require('./user_router'));

main_router.use('/student', require('./student_router'))

main_router.use('/code', require('./code_router'));

main_router.use('/bachelorean', require('./bachelorean_router'));

main_router.use('/collage', require('./collage_router'));

main_router.use('/sub-subject', require('./sub_subject_router'));

main_router.use('/year', require('./year_router'));

main_router.use('/coupon', require('./coupon_router'));

main_router.use('/question', require('./question_router'));

main_router.use('/answer' , require('./answer_router'))

main_router.use('/subject' , require('./subject_router'))

main_router.use('/previous' , require('./previous_router'))

main_router.use('/user-answer' , require('./user_answer_router'))

main_router.use('/university' , require('./university_router'))

main_router.use('/favorite' , require('./favorite_router'))

main_router.use('/note' , require('./note_router'))

main_router.use('/bachelors' , require('./bachelor_router'))

main_router.use('/links' , require('./link_router'))

main_router.use('/version' , require('./version_router'))


module.exports = main_router;