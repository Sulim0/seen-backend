const note_controller = require('../controllers/note_controller')
const note_router = require('express').Router()

note_router.post('/add-note' , note_controller.add_note)
note_router.post('/sync-notes/:id' , note_controller.sync_notes)

module.exports = note_router