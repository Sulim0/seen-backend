const previous_controller = require('../controllers/previous_controller')

const previous_router = require('express').Router()


previous_router.put('/get-previous/:id' , previous_controller.get_previous)
previous_router.put('/get-previouses-of-subject/:id' , previous_controller.get_previouses_of_subject)

module.exports = previous_router