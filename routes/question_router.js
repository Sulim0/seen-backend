const question_controller = require('../controllers/question_controller')

const question_router = require('express').Router()


question_router.put('/get-sub/:id' , question_controller.get_questions_of_sub_subject)
question_router.put('/get-previous/:id' , question_controller.get_questions_of_previous)
question_router.put('/get/:id' , question_controller.get_question)
question_router.post('/add-question' , question_controller.add_question)
question_router.post('/add-image/:id' , question_controller.upload.single('image') , question_controller.add_image_to_question)


module.exports = question_router