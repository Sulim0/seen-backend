const student_controller = require('../controllers/student_controller.js')

const student_router = require('express').Router()


//APIs

student_router.post('/add-student/:id' , student_controller.add_student)
student_router.patch('/update-student/:id' , student_controller.update_student)
student_router.delete('/delete-student/:id' , student_controller.delete_student)

//

module.exports = student_router