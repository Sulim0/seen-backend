const sub_subject_controller = require('../controllers/sub_subject_controller')

const sub_subject_router = require('express').Router()

sub_subject_router.put('/get-subs-of-subject/:id' , sub_subject_controller.get_subs_of_subject)

module.exports = sub_subject_router