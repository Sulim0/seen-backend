const subject_controller = require('../controllers/subject_controller')

const subject_router = require('express').Router()


subject_router.put('/get-subject/:id' , subject_controller.get_subject)
subject_router.put('/get-subjects-of-bachelor' , subject_controller.get_subjects_of_bachelor)
subject_router.put('/get-subjects-of-year' , subject_controller.get_subjects_of_year)

module.exports = subject_router