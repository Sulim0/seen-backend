const university_controller = require('../controllers/university_controller')

const university_router = require('express').Router()


university_router.get('/get-universities/:id' , university_controller.get_universities)


module.exports = university_router