const user_answer_controller = require('../controllers/user_answer_controller')

const user_answer_router = require('express').Router()


user_answer_router.post('/submit-answers/:id' , user_answer_controller.submit_answers)

user_answer_router.get('/get-user-answers/:id' , user_answer_controller.get_user_answers)

user_answer_router.put('/get-wrong-answers/:id' , user_answer_controller.get_user_wrong_answers)

user_answer_router.get('/get-wrong-answers-of-sub-subject/:id' , user_answer_controller.get_user_wrong_answers_of_sub_subject)



module.exports = user_answer_router