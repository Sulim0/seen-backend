const user_controller = require('../controllers/user_controller')

const user_router = require('express').Router()


//APIs

// user_router.post('/add-user' , user_controller.add_user)
user_router.put('/create-account/:id' , user_controller.create_account)
user_router.put('/update-user/:id' , user_controller.update_user)
user_router.delete('/delete-user/:id' , user_controller.delete_user)
user_router.put('/get-subjects/:id' , user_controller.get_user_subjects)
user_router.put('/get-active-subjects/:id' , user_controller.get_my_active_subjects)
user_router.put('/get-my-subjects/:id' , user_controller.get_my_subjects)
user_router.put('/get-my-codes/:id' , user_controller.get_user_codes)
user_router.put('/get-user-questions/:id' , user_controller.get_user_subjects_questions)
user_router.put('/get-user-subs/:id' , user_controller.get_user_sub_subjects)
user_router.put('/get-user-previouses/:id' , user_controller.get_user_previouses)
user_router.post('/login' , user_controller.user_login)
user_router.post('/add-image/:id' , user_controller.upload.single('image') , user_controller.add_image)
user_router.post('/sign-up' , user_controller.user_sign_up)
user_router.post('/sign-in' , user_controller.user_sign_in)




user_router.put('/chapter/:id' , user_controller.getChapter)


//

module.exports = user_router