const version_controller = require('../controllers/version_controller')
const version_router = require('express').Router()

version_router.get('/get-current-version' , version_controller.get_version)
version_router.post('/update-version' , version_controller.update_version)

module.exports = version_router