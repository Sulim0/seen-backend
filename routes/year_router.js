const year_controller = require('../controllers/year_controller')

const year_router = require('express').Router()

year_router.get('/get-years-of-collage/:id' , year_controller.get_years_of_collage)

module.exports = year_router