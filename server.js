const express = require('express');
const cron = require('node-cron');
const cors = require('cors')
const { sequelize, Code, Sequelize , User_coupon} = require('./utils/models')
const { Op } = require("sequelize");
const axios = require('axios');
const fs = require('fs');
const answer_controller = require('./controllers/answer_controller.js')
const url_update = answer_controller.url_update

const app = express()

const corsOptions = {
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    headers: ['Content-Type', 'Authorization'],
};

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static("./public/images/app"));
app.use('/public', express.static('./public'));
app.use((req, res, next) => {
  res.set('Cache-Control', 'no-store');
  next();
});
app.use(cors(corsOptions))

// for checking every 5 minutes
cron.schedule('*/5 * * * *', () => {
    console.log("//////////////////");
    url_update();
});

// ! for deleting codes after 2 years
cron.schedule('0 0 * * *', async () => {
    try {
        const codes = await Code.findAll({
            where: {
                user_id: {
                    [Sequelize.Op.ne]: null
                },
                expiry_time: {
                    [Sequelize.Op.ne]: 0
                },
                is_active: true
            }
        });
        codes.forEach( async (element) => {
            element.expiry_time -= 1;
            if (element.expiry_time === 0) {
                element.is_active = false;
            }
            await element.save();
        });
    } catch (error) {
        console.log(error.message);
    }
});


//! for deleting coupon
cron.schedule('0 0 * * *', async () => {
    try {
        const coupon = await User_coupon.findAll({
            where: {
                expiry_time: {
                    [Sequelize.Op.ne]: 0
                },
                is_expired: false
            }
        });
        coupon.forEach( async (element) => {
            element.expiry_time -= 1;
            if (element.expiry_time === 0) {
                element.is_expired = true;
            }
            await element.save();
        });
    } catch (error) {
        console.log(error.message);
    }
});



/// welcome API
app.get('/houdix/seen', (req, res) => {
    console.log("Hello from SEEN app!")
    return res.json("Hello from SEEN app!")
})

app.get('/', (req, res) => {
    console.log("it works!")
    return res.json("it works!")
})

//APIs
app.use('/houdix/seen/admin', require('./routes/admin/router'))

app.use('/houdix/seen/app' , require('./routes/main_router'))



// const passport = require('passport');
// var userProfile;

// app.use(passport.initialize());
// app.use(passport.session());

// app.get('/success', (req, res) => res.send(userProfile));
// app.get('/error', (req, res) => res.send("error logging in"));

// passport.serializeUser(function(user, cb) {
//   cb(null, user);
// });

// passport.deserializeUser(function(obj, cb) {
//   cb(null, obj);
// });

// /*  Google AUTH  */
 
// const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
// const GOOGLE_CLIENT_ID = '748314283562-pofotubn0beqeeci2vk7oqr18isdkbcq.apps.googleusercontent.com';
// const GOOGLE_CLIENT_SECRET = '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';
// passport.use(new GoogleStrategy({
//     clientID: GOOGLE_CLIENT_ID,
//     clientSecret: GOOGLE_CLIENT_SECRET,
//     callbackURL: "http://localhost:3000/auth/google/callback"
//   },
//   function(accessToken, refreshToken, profile, done) {
//       userProfile=profile;
//       return done(null, userProfile);
//   }
// ));
 
// app.get('/auth/google', 
//   passport.authenticate('google', { scope : ['profile', 'email'] }));
 
// app.get('/auth/google/callback', 
//   passport.authenticate('google', { failureRedirect: '/error' }),
//   function(req, res) {
//     // Successful authentication, redirect success.
//     res.redirect('/success');
//   });

//port
const PORT = process.env.PORT || 8080;
app.listen(PORT, async () => {
    //await sequelize.sync({ alter: true });
    console.log("server is on port " + PORT)
})