const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize , DataTypes) => {

    class Answer extends Model {
        static associate(models) {
            this.hasMany(models.User_answer, {
                foreignKey: 'answer_id',
                as: 'user_answer',
                allowNull : false,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Question, {
                foreignKey: 'question_id',
                as: 'question',
                allowNull: false,
                onDelete: 'CASCADE',
            }); 
        }
    }
    Answer.init({
        answer_content: {
          type: DataTypes.TEXT,
          allowNull: false
        },
        correctness: {
          type: DataTypes.BOOLEAN,
          allowNull : true
        },
        answer_notes: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        url: {
          type: DataTypes.TEXT,
          allowNull: true
        }
      }, {
        sequelize, 
        modelName: 'Answer',
        tableName: 'answer' 
      });

      return Answer
}