const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize , DataTypes) => {

    class Bachelor extends Model {
        static associate(models) {
            this.hasMany(models.Bachelorean, {
                foreignKey: 'bachelor_id',
                as: 'bachelorean',
                allowNull : false,
                onDelete: 'RESTRICT'
            });
            this.hasMany(models.Subject, {
                foreignKey: 'bachelor_id',
                as: 'subject',
                allowNull : true,
                onDelete: 'CASCADE'
            });
        }
    }
    Bachelor.init({
        bachelor_name: {
          type: DataTypes.STRING,
          allowNull: false
        }
      }, {
        sequelize, 
        modelName: 'Bachelor',
        tableName: 'bachelor' 
      });

      return Bachelor
}