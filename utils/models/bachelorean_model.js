const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Bachelorean extends Model {
        static associate(models) {
            this.belongsTo(models.Bachelor, {
                foreignKey: 'bachelor_id',
                as: 'bachelor',
                allowNull: false,
                onDelete: 'RESTRICT'
            });
            this.belongsTo(models.User, {
                foreignKey: {
                    name: "user_id",
                    allowNull: false
                },
                as: 'user',
                onDelete: "CASCADE",
            });
        }
    }
    Bachelorean.init({
        state: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'Bachelorean',
        tableName: 'bachelorean'
    });

    return Bachelorean
}