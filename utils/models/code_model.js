const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Code extends Model {
        static associate(models) {
            this.belongsToMany(models.Subject, {
                foreignKey: {
                    name: 'code_id',
                    allowNull: false
                },
                as: 'subjects',
                onDelete: 'CASCADE',
                through: models.Subject_code
            });
            this.hasMany(models.Subject_code , {
                foreignKey: 'code_id',
                as: 'subject_code',
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.User, {
                foreignKey: 'user_id',
                as: 'user',
                allowNull: true,
                onDelete: 'CASCADE'
            });
        }
    }
    Code.init({
        code_content: {
            type: DataTypes.UUID,
            unique: true,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
        },
        code_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        code_notes: {
            type: DataTypes.STRING,
            allowNull: true
        },
        expiry_time: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        date_of_activation: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: null
        },
        is_active: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        agent: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        price: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        m_university: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: ""
        }
    }, {
        sequelize,
        modelName: 'Code',
        tableName: 'code'
    });

    return Code
}