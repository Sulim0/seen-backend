const { Sequelize, Model } = require("sequelize");


module.exports = (sequelize , DataTypes) => {

    class Collage extends Model {
        static associate(models) {
            this.hasMany(models.Year, {
                foreignKey: {
                  name: "collage_id",
                  allowNull: false
                },
                as: 'year',
                onDelete: "CASCADE",
            });
            this.belongsTo(models.University, {
                foreignKey: {
                  name:'university_id',
                  allowNull: false
                },
                as: 'university',
                onDelete: 'CASCADE'
            }); 
        }
    }
    Collage.init({
        collage_name: {
          type: DataTypes.STRING,
          allowNull: false
        }
      }, {
        sequelize, 
        modelName: 'Collage',
        tableName: 'collage' 
      });

      return Collage
}