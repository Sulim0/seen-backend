const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Coupon extends Model {
        static associate(models) {
            this.hasMany(models.User_coupon, {
                foreignKey: 'coupon_id',
                as: 'user_coupon',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.belongsToMany(models.Subject, {
                foreignKey: {
                    name: 'coupon_id',
                    allowNull: false
                },
                as: 'subjects',
                onDelete: 'CASCADE',
                through: models.Subject_coupon
            });
            this.hasMany(models.Subject_coupon , {
                foreignKey: 'coupon_id',
                as: 'subject_coupon',
                allowNull: false,
                onDelete: 'CASCADE'
            });
        }
    }
    Coupon.init({
        coupon_content: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        coupon_notes: {
            type: DataTypes.STRING,
            allowNull: false
        },
        is_active: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        expiry_time: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        limit: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        agent: {
            type: DataTypes.TEXT,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'Coupon',
        tableName: 'coupon'
    });

    return Coupon
}