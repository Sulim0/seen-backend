const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Favorite extends Model {
        static associate(models) {
            this.belongsTo(models.Question , {
                foreignKey: 'question_id',
                as: 'question',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.User , {
                foreignKey: 'user_id',
                as: 'user',
                allowNull: false,
                onDelete: 'CASCADE'
            })
        }
    }
    Favorite.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'Favorite',
        tableName: 'favorite'
    });

    return Favorite
}