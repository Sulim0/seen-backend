'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Link extends Model {
        static associate(models) { }
    }
    Link.init(
        {
            url: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
            description: {
                type: DataTypes.STRING,
                allowNull: true,
            }
        },
        {
            sequelize,
            modelName: 'Link',
            tableName: 'link',
        }
    );
    return Link;
};
