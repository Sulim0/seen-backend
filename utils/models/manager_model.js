const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Manager extends Model {
        static associate(models) {
        }
    }
    Manager.init({
        user_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        verification_code: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
    }, {
        sequelize,
        modelName: 'Manager',
        tableName: 'manager'
    });

    return Manager
}