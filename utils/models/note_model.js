const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Note extends Model {
        static associate(models) {
            this.belongsTo(models.User, {
                foreignKey: 'user_id',
                as: 'user',
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Question, {
                foreignKey: 'question_id',
                as: 'questuion',
                onDelete: 'CASCADE'
            });
        }
    }
    Note.init({
        note: {
            type: DataTypes.TEXT,
            allowNull: false
        },
    }, {
        sequelize,
        modelName: 'Note',
        tableName: 'note'
    });

    return Note;
}