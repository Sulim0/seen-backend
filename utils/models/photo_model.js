const { Model } = require('sequelize'); 
module.exports = (sequelize, DataTypes) => { 
    class Photo extends Model { 
        static associate(models) { 
            this.belongsTo(models.Question, { 
                foreignKey: 'question_id', 
                as: "question" 
            }); 
            this.belongsTo(models.Answer, { 
                foreignKey: 'answer_id', 
                as: "answer" 
            }); 
        } 
    } 
    Photo.init( 
        { 
            url: { 
                type: DataTypes.STRING, 
                allowNull: true, 
                defaultValue: null, 
            } 
        }, 
        { 
            sequelize, 
            modelName: 'Photo', 
            tableName: 'photos' 
        } 
    ); 
    return Photo; 
};