const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize , DataTypes) => {

    class Previous extends Model {
        static associate(models) {
            // this.hasMany(models.Question, {
            //     foreignKey: 'previous_id',
            //     as: 'question',
            // });
            this.hasMany(models.Previous_question , {
              foreignKey: 'previous_id',
              as: 'previous_questions',
              onDelete: 'CASCADE'
            });
            this.belongsTo(models.Subject, {
                foreignKey: 'subject_id',
                as: 'subject',
                onDelete: 'CASCADE'
            }); 
        }
    }
    Previous.init({
        previous_name: {
          type: DataTypes.STRING,
          allowNull: false
        },
        previous_notes: {
          type: DataTypes.TEXT,
          allowNull : true
        },
        sort: {
          type: DataTypes.INTEGER,
          allowNull: true,
        }
      }, {
        sequelize, 
        modelName: 'Previous',
        tableName: 'previous' 
      });

      return Previous
}