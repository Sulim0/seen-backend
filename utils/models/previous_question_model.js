
const { Sequelize , Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Previous_question extends Model {
        static associate(models) {
            this.belongsTo(models.Previous, {
                foreignKey: 'previous_id',
                as: 'previous',
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Question, {
                foreignKey: 'question_id',
                as: 'question',
                onDelete: 'CASCADE'
            });
        }
    }
    Previous_question.init(
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
            }
        },
        {
            sequelize,
            modelName: 'Previous_question',
            tableName: 'previous_question',
        }
    );
    return Previous_question;
};