const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

  class Question extends Model {
    static associate(models) {
      this.hasMany(models.Answer, {
        foreignKey: 'question_id',
        as: 'answer',
        allowNull: false,
        onDelete: 'CASCADE',
      });
      // this.belongsTo(models.Previous, {
      //   foreignKey: 'previous_id',
      //   as: 'previous',
      //   allowNull: true
      // });
      this.hasMany(models.Previous_question , {
          foreignKey: 'question_id',
          as: 'previous_questions',
          onDelete: 'CASCADE'
      });
      this.hasMany(models.Sub_subject_question , {
        foreignKey: 'question_id',
        as: 'sub_subject_questions',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Note, {
        foreignKey: 'question_id',
        as: 'notes',
        onDelete: 'CASCADE'
      });
    }
  }
  Question.init({
    question_content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    question_notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    is_mcq: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    is_rollable: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    url: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Question',
    tableName: 'question'
  });

  return Question
}