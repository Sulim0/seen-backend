const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize , DataTypes) => {

    class Student extends Model {
        static associate(models) {
            this.belongsTo(models.User, {
                foreignKey: {
                  name: "user_id",
                  allowNull: false
                },
                as: 'user',
                onDelete: "CASCADE",
            });
            this.belongsTo(models.Year, {
                foreignKey: 'year_id',
                as: 'year',
                onDelete: 'RESTRICT',
            }); 
        }
    }
    Student.init({
        student_number: {
          type: DataTypes.STRING,
          allowNull: true
        }
      }, {
        sequelize, 
        modelName: 'Student',
        tableName: 'student' 
      });

      return Student
}