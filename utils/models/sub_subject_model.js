const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

  class Sub_subject extends Model {
    static associate(models) {
      this.belongsTo(models.Sub_subject, {
        foreignKey: {
          name: 'father_id',
          allowNull: true
        },
        as: 'parent',
        onDelete: 'CASCADE',
      });
      this.hasMany(models.Sub_subject, {
        foreignKey: {
          name: 'father_id',
          allowNull: true
        },
        as: "children",
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Subject, {
        foreignKey: 'subject_id',
        as: 'subject',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Sub_subject_question, {
        foreignKey: 'sub_subject_id',
        as: 'sub_subject_questions',
        onDelete: 'CASCADE'
      });
    }
  }
  Sub_subject.init({
    sub_subject_name: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    sub_subject_notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    sort: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    modelName: 'Sub_subject',
    tableName: 'sub_subject'
  });

  return Sub_subject
}