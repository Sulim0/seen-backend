
const { Sequelize , Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Sub_subject_question extends Model {
        static associate(models) {
            this.belongsTo(models.Sub_subject, {
                foreignKey: 'sub_subject_id',
                as: 'sub_subject',
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Question, {
                foreignKey: 'question_id',
                as: 'question',
                onDelete: 'CASCADE'
            });
        }
    }
    Sub_subject_question.init(
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
            }
        },
        {
            sequelize,
            modelName: 'Sub_subject_question',
            tableName: 'sub_subject_question',
        }
    );
    return Sub_subject_question;
};
