const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Subject_code extends Model {
        static associate(models) {
            this.belongsTo(models.Subject , {
                foreignKey: 'subject_id',
                as: 'subject',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Code , {
                foreignKey: 'code_id',
                as: 'code',
                allowNull: false,
                onDelete: 'CASCADE'
            });
        }
    }
    Subject_code.init({
        // id: {
        //     type: DataTypes.INTEGER,
        //     primaryKey: true,
        //     autoIncrement: true,
        //     allowNull: false
        // }
    }, {
        sequelize,
        modelName: 'Subject_code',
        tableName: 'subject_code'
    });

    return Subject_code
}