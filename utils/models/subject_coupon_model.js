const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Subject_coupon extends Model {
        static associate(models) {
            this.belongsTo(models.Subject , {
                foreignKey: 'subject_id',
                as: 'subject',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Coupon , {
                foreignKey: 'coupon_id',
                as: 'coupon',
                allowNull: false,
                onDelete: 'CASCADE'
            });
        }
    }
    Subject_coupon.init({
        // id: {
        //     type: DataTypes.INTEGER,
        //     primaryKey: true,
        //     autoIncrement: true,
        //     allowNull: false
        // }
    }, {
        sequelize,
        modelName: 'Subject_coupon',
        tableName: 'subject_coupon'
    });

    return Subject_coupon
}