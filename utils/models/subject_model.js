const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Subject extends Model {
        static associate(models) {
            this.hasMany(models.Sub_subject, {
                foreignKey: {
                    name: 'subject_id',
                    allowNull: true
                },
                as: 'sub_subject',
                onDelete: "CASCADE"
            });
            this.hasMany(models.Previous, {
                foreignKey: {
                    name: "subject_id",
                    allowNull: false
                },
                as: 'previous',
                onDelete: "CASCADE"
            });
            this.belongsToMany(models.Code, {
                foreignKey: {
                    name: 'subject_id',
                    allowNull: false
                },
                as: 'codes',
                onDelete: 'CASCADE',
                through: models.Subject_code
            });
            this.hasMany(models.Subject_code , {
                foreignKey: 'subject_id',
                as: 'subject_code',
                onDelete: 'CASCADE'
            });
            this.hasMany(models.Subject_coupon , {
                foreignKey: 'subject_id',
                as: 'subject_coupon',
                onDelete: 'CASCADE'
            });
            this.belongsToMany(models.Coupon, {
                foreignKey: {
                    name: 'subject_id',
                    allowNull: false
                },
                as: 'coupons',
                onDelete: 'CASCADE',
                through: models.Subject_coupon
            });
            this.belongsTo(models.Bachelor, {
                foreignKey: 'bachelor_id',
                as: 'bachelor',
                allowNull: true,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Year, {
                foreignKey: 'year_id',
                as: 'year',
                allowNull: true,
                onDelete: 'CASCADE'
            });
        }
    }
    Subject.init({
        subject_name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        subject_notes: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        term: {
            type: DataTypes.STRING,
            allowNull: false
        },
        language: {
            type: DataTypes.STRING,
            allowNull: false
        },

    }, {
        sequelize,
        modelName: 'Subject',
        tableName: 'subject'
    });

    return Subject
}