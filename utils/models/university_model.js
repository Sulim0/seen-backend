const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize , DataTypes) => {

    class University extends Model {
        static associate(models) {
            this.hasMany(models.Collage, {
                foreignKey: {
                  name: 'university_id',
                  allowNull: false,
                },
                as: 'collage',
                onDelete: 'CASCADE',
            });
        }
    }    
    University.init({
        university_name: {
          type: DataTypes.STRING,
          allowNull: false
        },
        university_state: {
          type: DataTypes.STRING,
          allowNull: true
        }
      }, {
        sequelize, 
        modelName: 'University',
        tableName: 'university' 
      });

      return University
}