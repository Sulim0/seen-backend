const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class User_answer extends Model {
        static associate(models) {
            this.belongsTo(models.User, {
                foreignKey: 'user_id',
                as: 'user',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Answer, {
                foreignKey: 'answer_id',
                as: 'answer',
                allowNull: false,
                onDelete: 'CASCADE'
            });
        }
    }
    User_answer.init({
        is_correct: {
            type: DataTypes.BOOLEAN,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'User_answer',
        tableName: 'user_answer'
    });

    return User_answer
}