const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize , DataTypes) => {

    class User_coupon extends Model {
        static associate(models) {
            this.belongsTo(models.User, {
                foreignKey: 'user_id',
                as: 'user',
                allowNull : false,
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Coupon, {
                foreignKey: 'coupon_id',
                as: 'coupon',
                allowNull: false,
                onDelete: 'CASCADE'
            }); 
        }
    }
    User_coupon.init({
        is_expired: {
          type: DataTypes.BOOLEAN,
          allowNull: false
        },
        expiry_time: {
          type: DataTypes.INTEGER,
          allowNull: false
        }
      }, {
        sequelize, 
        modelName: 'User_coupon',
        tableName: 'user_coupon' 
      });

      return User_coupon
}