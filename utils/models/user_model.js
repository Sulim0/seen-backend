const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class User extends Model {
        static associate(models) {
            this.hasMany(models.Note, {
                foreignKey: 'user_id',
                as: 'notes',
                onDelete: 'CASCADE'
            });
            this.hasOne(models.Student, {
                foreignKey: {
                    name: 'user_id',
                    allowNull: false
                },
                as: 'student',
                onDelete: 'CASCADE'
            });
            this.hasOne(models.Bachelorean, {
                foreignKey: {
                    name: "user_id",
                    allowNull: false
                },
                as: 'bachelorean',
                onDelete: 'CASCADE'
            });
            this.hasMany(models.User_answer, {
                foreignKey: 'user_id',
                as: 'user_answer',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.hasMany(models.User_coupon, {
                foreignKey: 'user_id',
                as: 'user_coupon',
                allowNull: false,
                onDelete: 'CASCADE'
            });
            this.hasMany(models.Code, {
                foreignKey: 'user_id',
                as: 'codes',
                allowNull: false,
                onDelete: 'CASCADE'
            });
        }
    }
    User.init({
        user_fullname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        user_username: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'default_user'
        },
        user_password: {
            type: DataTypes.STRING,
            allowNull: true
        },
        user_email: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: true
        },
        user_phonenumber: {
            type: DataTypes.STRING,
            allowNull: true//,
            // unique: true
        },
        user_type: {
            type: DataTypes.ENUM,
            values: ["student", "bachelorean" , "none"],
            allowNull: false,
        },
        is_logged_in: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        user_image: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        user_token: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        sex: {
            type: DataTypes.ENUM,
            allowNull: true,
            values: ["male" , "female"]
        },
        userSubId: {
            type: DataTypes.TEXT,
            allowNull: false,
        }
    }, {
        sequelize,
        modelName: 'User',
        tableName: 'user'
    });

    return User
}