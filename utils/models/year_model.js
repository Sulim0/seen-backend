const { Sequelize, Model } = require("sequelize")

module.exports = (sequelize, DataTypes) => {

    class Year extends Model {
        static associate(models) {
            this.hasMany(models.Student, {
                foreignKey: 'user_id',
                as: 'student',
                onDelete: 'RESTRICT'
            });
            this.hasMany(models.Subject, {
                foreignKey: {
                    name: "year_id",
                    allowNull: true
                },
                as: 'subject',
                onDelete: 'CASCADE'
            });
            this.belongsTo(models.Collage, {
                foreignKey: 'collage_id',
                as: 'collage',
                onDelete: 'CASCADE'
            });
        }
    }
    Year.init({
        year_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        is_root: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        sequelize,
        modelName: 'Year',
        tableName: 'year'
    });

    return Year
}