require('dotenv').config();
const nodemailer = require('nodemailer');

exports.createTransport = async (user_name, code) => {
    // node mailer setting
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.MY_EMAIL,
            pass: process.env.SEND_EMAILS_PASSWORD
        }
    });
    const info = await transporter.sendMail({
        from: {
            name: "seen app",
            address: process.env.MY_EMAIL
        },
        to: process.env.MY_EMAIL,
        subject: `${user_name}`,
        text: `the verification code is ${code}`
    });
    return info;
};